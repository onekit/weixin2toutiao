/* eslint-disable camelcase */
export default class WxSocketTask {
  constructor(tt_socketTask) {
    this.tt_socketTask = tt_socketTask
  }

  onMessage() {
    if (getApp().onekit_onSocketonMessage) {
      getApp().onekit_onSocketonMessage()
    }
    return this.tt_socketTask.onMessage()
  }

  onClose() {
    if (getApp().onekit_onSocketon) {
      getApp().onekit_onSocketonClose()
    }
    return this.tt_socketTask.onClose()
  }

  onError() {
    if (getApp().onekit_onSocketonError) {
      getApp().onekit_onSocketonError()
    }
    return this.tt_socketTask.onError()
  }

  onOpen() {
    if (getApp().onekit_onSocketonOpen) {
      getApp().onekit_onSocketonOpen()
    }
    return this.tt_socketTask.onOpen()
  }

  close() {
    if (getApp().onekit_onSocketClose) {
      getApp().onekit_onSocketClose()
    }
    return this.tt_socketTask.close()
  }

  send() {
    if (getApp().onekit_onSocketSend) {
      getApp().onekit_onSocketSend()
    }
    return this.tt_socketTask.send()
  }
}
