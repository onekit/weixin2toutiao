/* eslint-disable no-undef */
/* eslint-disable prefer-spread */
/* eslint-disable no-console */
/* eslint-disable max-len */
/* eslint-disable camelcase */
import CanvasContext from './api/CanvasContext'
// import VideoContext from './api/VideoContext'
// import LivePlayerContext from './api/LivePlayerContext'
import WORKER from './api/WORKER'
import WxCloud from './wx.cloud'
import onekit from './js/onekit'
import WxSocketTask from './api/WxSocketTask'

export default class wx {
  // ///////////////// animation //////////////////////////
  static createAnimation(wx_object) {
    return tt.createAnimation(wx_object)
  }

  // /////////////// basic ////////////////////////////////
  static canIUse() {
    return true
  }

  static _getSystemInfo(wx_object) {
    return tt._getSystemInfo(wx_object)
  }

  static getSystemInfo(wx_object) {
    return tt.getSystemInfo(wx_object)
  }

  static getSystemInfoSync(wx_object) {
    return tt.getSystemInfoSync(wx_object)
  }

  static base64ToArrayBuffer(base64) {
    return tt.base64ToArrayBuffer(base64)
  }

  static arrayBufferToBase64(arrayBuffer) {
    return tt.arrayBufferToBase64(arrayBuffer)
  }

  static getEnterOptionsSync(wx_object) {
    return tt.getEnterOptionsSync(wx_object)
  }

  static getUpdateManager(wx_object) {
    return tt.getUpdateManager(wx_object)
  }

  static getLaunchOptionsSync(wx_object) {
    return tt.getLaunchOptionsSync(wx_object)
  }

  static exitMiniProgram(wx_object) {
    return tt.exitMiniProgram(wx_object)
  }

  static offPageNotFound(wx_object) {
    return tt.offPageNotFound(wx_object)
  }

  static onPageNotFound(wx_object) {
    return tt.onPageNotFound(wx_object)
  }

  static offError(wx_object) {
    return tt.offError(wx_object)
  }

  static onError(wx_object) {
    return tt.onError(wx_object)
  }

  static offAppShow(wx_object) {
    return tt.offAppShow(wx_object)
  }

  static onAppShow(wx_object) {
    return tt.onAppShow(wx_object)
  }

  static offAppHide(wx_object) {
    return tt.offAppHide(wx_object)
  }

  static onAppHide(wx_object) {
    return tt.onAppHide(wx_object)
  }

  static setEnableDebug(wx_object) {
    return tt.setEnableDebug(wx_object)
  }

  static getLogManager(wx_object) {
    return tt.getLogManager(wx_object)
  }

  // ///////////////// Canvas ///////////////////
  // eslint-disable-next-line complexity
  static drawCanvas(wx_object) {
    const canvasId = wx_object.canvasId
    const actions = wx_object.actions
    const canvasContext = tt.createCanvasContext(canvasId)
    for (const action of actions) {
      const data = action.data
      switch (action.method) {
        case 'save':
          canvasContext.save()
          break
        case 'restore':
          canvasContext.restore()
          break
        case 'setFillStyle':
          canvasContext.setFillStyle(onekit.color.array2str(data[1]))
          break
        case 'setStrokeStyle':
          canvasContext.setStrokeStyle(onekit.color.array2str(data[1]))
          break
        case 'setFontSize':
          canvasContext.setFontSize(data[0])
          break
        case 'setGlobalAlpha':
          canvasContext.setGlobalAlpha(data[0])
          break
        case 'setShadow':
          canvasContext.setShadow(data[0], data[1], data[2], onekit.color.array2str(data[3]))

          break
        case 'drawImage':
          canvasContext.drawImage.apply(canvasContext, data)
          break
        case 'fillText':
          canvasContext.fillText.apply(canvasContext, data)
          break
        case 'setLineCap':
          canvasContext.setLineCap(data[0])
          break
        case 'setLineJoin':
          canvasContext.setLineJoin(data[0])
          break
        case 'setLineWidth':
          canvasContext.setLineWidth(data[0])
          break
        case 'setMiterLimit':
          canvasContext.setMiterLimit(data[0])
          break
        case 'rotate':
          canvasContext.rotate(data[0])
          break
        case 'scale':
          canvasContext.scale(data[0], data[1])
          break
        case 'translate':
          canvasContext.translate(data[0], data[1])
          break
        case 'strokePath':
          canvasContext.beginPath()
          for (const dat of data) {
            const dt = dat.data
            switch (dat.method) {
              case 'rect':
                canvasContext.strokeRect(dt[0], dt[1], dt[2], dt[3])
                break
              case 'moveTo':
                canvasContext.moveTo(dt[0], dt[1])
                break
              case 'lineTo':
                canvasContext.lineTo(dt[0], dt[1])
                break
              case 'closePath':
                canvasContext.closePath()
                break
              case 'arc':
                canvasContext.arc.apply(canvasContext, dt)
                break
              case 'quadraticCurveTo':
                canvasContext.quadraticCurveTo.apply(canvasContext, dt)
                break
              case 'bezierCurveTo':
                canvasContext.bezierCurveTo.apply(canvasContext, dt)
                break

              default:
                console.log('[drawCanvas-strokePath]', dat.method)
                break
            }
          }
          canvasContext.stroke()
          break
        case 'fillPath':
          for (const dat of data) {
            const dt = dat.data
            switch (dat.method) {
              case 'rect':
                canvasContext.fillRect(dt[0], dt[1], dt[2], dt[3])
                break
              case 'arc':
                canvasContext.arc.apply(canvasContext, dt)
                break
              default:
                console.log('[drawCanvas-fillPath]', dat.method)
                break
            }
          }
          canvasContext.fill()
          break
        case 'clearRect':
          canvasContext.clearRect(data[0], data[1], data[2], data[3])
          break
        default:
          console.log('[drawCanvas]', action.method)
          break
      }
    }
    canvasContext.draw()
  }

  static createContext() {
    const context = new CanvasContext()
    return context
  }

  static createCanvasContext(wx_object) {
    return tt.createCanvasContext(wx_object)
  }

  static canvasToTempFilePath(wx_object) {
    return tt.canvasToTempFilePath(wx_object)
  }

  static canvasPutImageData(wx_object) {
    return tt.canvasPutImageData(wx_object)
  }

  static canvasGetImageData(wx_object) {
    return tt.canvasGetImageData(wx_object)
  }

  // //////////// Device //////////////////
  static onBeaconServiceChange(wx_object) {
    return tt.onBeaconServiceChange(wx_object)
  }

  static onBeaconUpdate(wx_object) {
    return tt.onBeaconUpdate(wx_object)
  }

  static getBeacons(wx_object) {
    return tt.getBeacons(wx_object)
  }

  static stopBeaconDiscovery() {
    /* return tt.stopBeaconDiscovery(wx_object); */
  }

  static startBeaconDiscovery() {
    /* return tt.startBeaconDiscovery(wx_object); */
  }

  static stopWifi() {
    /* return tt.stopWifi(wx_object); */
  }

  static startWifi() {
    /* return tt.startWifi(wx_object); */
  }

  static setWifiList(wx_object) {
    return tt.setWifiList(wx_object)
  }

  static createMediaContainer() {
    return console.warn('.createMediaContainer is not suport')
  }

  static onWifiConnected(wx_callback) {
    return tt.onWifiConnected(wx_callback)
  }

  static onGetWifiList(wx_callback) {
    return tt.onGetWifiList(wx_callback)
  }

  static offGetWifiList(wx_callback) {
    return tt.offGetWifiList(wx_callback)
  }

  static getWifiList(wx_object) {
    return tt.getWifiList(wx_object)
  }

  static getConnectedWifi(wx_object) {
    return tt.getConnectedWifi(wx_object)
  }

  static connectWifi(wx_object) {
    return tt.connectWifi(wx_object)
  }

  //
  static onAccelerometerChange(callback) {
    return tt.onAccelerometerChange(callback)
  }

  static stopAccelerometer(wx_object) {
    return tt.stopAccelerometer(wx_object)
  }

  static startAccelerometer(wx_object) {
    return tt.startAccelerometer(wx_object)
  }

  static getBatteryInfoSync(wx_object) {
    return tt.getBatteryInfoSync(wx_object)
  }

  static getBatteryInfo() {
    return console.warn('getBatteryInfo is not support')
  }

  //
  static getClipboardData(wx_object) {
    return tt.getClipboardData(wx_object)
  }

  static setClipboardData(wx_object) {
    return tt.setClipboardData(wx_object)
  }

  static onCompassChange(callback) {
    return tt.onCompassChange(callback)
  }

  static stopCompass(wx_object) {
    return tt.stopCompass(wx_object)
  }

  static onBLEPeripheralConnectionStateChanged() {
    return console.warn('onBLEPeripheralConnectionStateChanged is not suport')
  }

  static startCompass(wx_object) {
    return tt.startCompass(wx_object)
  }

  static addPhoneContact() {
    /* return tt.addPhoneContact(wx_object); */
  }

  static onGyroscopeChange(callback) {
    return tt.onGyroscopeChange(callback)
  }

  static stopGyroscope(wx_object) {
    return tt.stopGyroscope(wx_object)
  }

  static startGyroscope(wx_object) {
    return tt.startGyroscope(wx_object)
  }

  //
  static onDeviceMotionChange(wx_object) {
    return tt.onDeviceMotionChange(wx_object)
  }

  static onSocketonOpen(wx_callback) {
    getApp().onekit_onSocketonOpen = wx_callback
  }

  static onSocketonMessage(wx_callback) {
    getApp().onekit_onSocketonMessage = wx_callback
  }

  static createUDPSocket() {
    return console.warn('createUDPSocket is not suport')
  }

  static onSocketonError(wx_callback) {
    getApp().onekit_onSocketonError = wx_callback
  }

  static onSocketClose(wx_callback) {
    getApp().onekit_onSocketClose = wx_callback
  }

  static onSocketonClose(wx_callback) {
    getApp().onekit_onSocketonClose = wx_callback
  }

  static onSocketSend(wx_callback) {
    getApp().onekit_onSocketSend = wx_callback
  }

  static stopDeviceMotionListening(wx_object) {
    return tt.stopDeviceMotionListening(wx_object)
  }

  static startDeviceMotionListening(wx_object) {
    return tt.startDeviceMotionListening(wx_object)
  }


  //
  static getNetworkType(wx_object) {
    return tt.getNetworkType(wx_object)
  }

  static _network(res) {
    return tt._network(res)
  }

  static onNetworkStatusChange(callack) {
    return tt.onNetworkStatusChange(callack)
  }

  //
  static makePhoneCall(wx_object) {
    return tt.makePhoneCall(wx_object)
  }

  static scanCode(wx_object) {
    return tt.scanCode(wx_object)
  }

  //
  static vibrateLong(wx_object) {
    return tt.vibrateLong(wx_object)
  }

  static vibrateShort(wx_object) {
    return tt.vibrateShort(wx_object)
  }

  //
  static onMemoryWarning(wx_object) {
    return tt.onMemoryWarning(wx_object)
  }

  //
  static writeBLECharacteristicValue(wx_object) {
    return tt.writeBLECharacteristicValue(wx_object)
  }

  static readBLECharacteristicValue(wx_object) {
    return tt.readBLECharacteristicValue(wx_object)
  }

  static onBLEConnectionStateChange(wx_object) {
    return tt.onBLEConnectionStateChange(wx_object)
  }

  static onBLECharacteristicValueChange(wx_object) {
    return tt.onBLECharacteristicValueChange(wx_object)
  }

  static notifyBLECharacteristicValueChange(wx_object) {
    return tt.notifyBLECharacteristicValueChange(wx_object)
  }

  static getBLEDeviceServices(wx_object) {
    return tt.getBLEDeviceServices(wx_object)
  }

  static getBLEDeviceCharacteristics(wx_object) {
    return tt.getBLEDeviceCharacteristics(wx_object)
  }

  static createBLEConnection(wx_object) {
    return tt.createBLEConnection(wx_object)
  }

  static closeBLEConnection(wx_object) {
    return tt.closeBLEConnection(wx_object)
  }

  //
  static stopBluetoothDevicesDiscovery() {
    /* return tt.stopBluetoothDevicesDiscovery(wx_object); */
  }

  static startBluetoothDevicesDiscovery() {
    /* return tt.startBluetoothDevicesDiscovery(wx_object) */
  }

  static openBluetoothAdapter() {
    /* return tt.openBluetoothAdapter(wx_object); */
  }

  static onBluetoothDeviceFound() {
    /* return tt.onBluetoothDeviceFound(wx_object) */
  }

  static onBluetoothAdapterStateChange(wx_object) {
    return tt.onBluetoothAdapterStateChange(wx_object)
  }

  static getConnectedBluetoothDevices(wx_object) {
    return tt.getConnectedBluetoothDevices(wx_object)
  }

  static getBluetoothDevices(wx_object) {
    return tt.getBluetoothDevices(wx_object)
  }

  static getBluetoothAdapterState() {
    /* return tt.getBluetoothAdapterState(wx_object) */
  }

  static closeBluetoothAdapter() {
    /* return tt.closeBluetoothAdapter(wx_object); */
  }

  //
  static stopHCE(wx_object) {
    return tt.stopHCE(wx_object)
  }

  static startHCE(wx_object) {
    return tt.startHCE(wx_object)
  }

  static sendHCEMessage(wx_object) {
    return tt.sendHCEMessage(wx_object)
  }

  static onHCEMessage(wx_object) {
    return tt.onHCEMessage(wx_object)
  }

  static getHCEState(wx_object) {
    return tt.getHCEState(wx_object)
  }

  //
  static setScreenBrightness(wx_object) {
    return tt.setScreenBrightness(wx_object)
  }

  static setKeepScreenOn(wx_object) {
    return tt.setKeepScreenOn(wx_object)
  }

  static onUserCaptureScreen(wx_object) {
    return tt.onUserCaptureScreen(wx_object)
  }

  static offUserCaptureScreen(wx_object) {
    return tt.offUserCaptureScreen(wx_object)
  }

  static getScreenBrightness(wx_object) {
    return tt.getScreenBrightness(wx_object)
  }

  // ///////////////// Ext //////////////
  static getExtConfigSync(wx_object) {
    return tt.getExtConfigSync(wx_object)
  }

  static getExtConfig(wx_object) {
    return tt.getExtConfig(wx_object)
  }

  // ////////////////// File //////////
  static getFileSystemManager(wx_object) {
    return tt.getFileSystemManager(wx_object)
  }

  static getFileInfo(wx_object) {
    return tt.getFileInfo(wx_object)
  }

  static removeSavedFile(wx_object) {
    return tt.removeSavedFile(wx_object)
  }

  static getSavedFileInfo(wx_object) {
    return tt.getSavedFileInfo(wx_object)
  }

  static getSavedFileList(wx_object) {
    return tt.getSavedFileList(wx_object)
  }

  static openDocument(wx_object) {
    return tt.openDocument(wx_object)
  }

  static saveFile(wx_object) {
    return tt.saveFile(wx_object)
  }

  // ////////// Location ///////////////
  static openLocation(wx_object) {
    return tt.openLocation(wx_object)
  }

  static getLocation(wx_object) {
    return tt.getLocation(wx_object)
  }

  static chooseLocation(wx_object) {
    return tt.chooseLocation(wx_object)
  }

  // //////// Media ////////////////////
  static createMapContext(id) {
    return getApp().onekit.context[id]
  }

  static compressImage(wx_object) {
    return tt.compressImage(wx_object)
  }

  static saveImageToPhotosAlbum(wx_object) {
    return tt.saveImageToPhotosAlbum(wx_object)
  }

  static getImageInfo(wx_object) {
    return tt.getImageInfo(wx_object)
  }

  static previewImage(wx_object) {
    return tt.previewImage(wx_object)
  }

  static chooseImage(wx_object) {
    return tt.chooseImage(wx_object)
  }

  static saveVideoToPhotosAlbum(wx_object) {
    return tt.saveVideoToPhotosAlbum(wx_object)
  }

  static chooseVideo(wx_object) {
    return tt.chooseVideo(wx_object)
  }

  static createVideoContext(wx_object) {
    return tt.createVideoContext(wx_object)
  }

  static createLivePlayerContext(wx_object) {
    return tt.createLivePlayerContext(wx_object)
  }

  static createCameraContext(wx_object) {
    return tt.createCameraContext(wx_object)
  }

  static stopVoice(wx_object) {
    return tt.stopVoice(wx_object)
  }

  static pauseVoice(wx_object) {
    return tt.pauseVoice(wx_object)
  }

  static playVoice(wx_object) {
    return tt.playVoice(wx_object)
  }

  static setInnerAudioOption(wx_object) {
    return tt.setInnerAudioOption(wx_object)
  }

  static getAvailableAudioSources(wx_object) {
    return tt.getAvailableAudioSources(wx_object)
  }

  static createInnerAudioContext(wx_object) {
    return tt.createInnerAudioContext(wx_object)
  }

  // static createAudioContext(wx_object) {
  //   return tt.createAudioContext(wx_object)
  // }

  static onBackgroundAudioStop(wx_object) {
    return tt.onBackgroundAudioStop(wx_object)
  }

  static onBackgroundAudioPause(wx_object) {
    return tt.onBackgroundAudioPause(wx_object)
  }

  static onBackgroundAudioPlay(wx_object) {
    return tt.onBackgroundAudioPlay(wx_object)
  }

  static stopBackgroundAudio(wx_object) {
    return tt.stopBackgroundAudio(wx_object)
  }

  static seekBackgroundAudio(wx_object) {
    return tt.seekBackgroundAudio(wx_object)
  }

  static pauseBackgroundAudio(wx_object) {
    return tt.pauseBackgroundAudio(wx_object)
  }

  static playBackgroundAudio() {
    /* return tt.playBackgroundAudio(wx_object) */
  }

  static getBackgroundAudioPlayerState() {
    /* return tt.getBackgroundAudioPlayerState(wx_object) */
  }

  static getBackgroundAudioManager(wx_object) {
    return tt.getBackgroundAudioManager(wx_object)
  }

  static createLivePusherContext(wx_object) {
    return tt.createLivePusherContext(wx_object)
  }

  static startRecord(wx_object) {
    const recorderManager = tt.getRecorderManager(wx_object)
    recorderManager.onStart(() => {
      const res = 'stopRecord才会返回tempFilePath!!'
      if (wx_object.success) {
        wx_object.success(res)
      }
      if (wx_object.complete) {
        wx_object.complete(res)
      }
    })
    const result = recorderManager.start()
    return result
  }

  static stopRecord(wx_object) {
    const recorderManager = tt.getRecorderManager(wx_object)
    recorderManager.onStop((res) => {
      if (wx_object.success) {
        wx_object.success(res)
      }
      if (wx_object.complete) {
        wx_object.complete(res)
      }
    })
    const result = recorderManager.stop()
    return result
  }

  static getRecorderManager(wx_object) {
    return tt.getRecorderManager(wx_object)
  }

  // ////////////// Network ///////////////
  static request(wx_object) {
    return tt.request(wx_object)
  }

  static downloadFile(wx_object) {
    return tt.downloadFile(wx_object)
  }

  static uploadFile(wx_object) {
    // tt.uploadFile({
    //   url: wx_object.url,
    //   filePath: wx_object.filePath,
    //   fileName: wx_object.name,
    //   fileType: 'image',
    //   header: wx_object.header,
    //   formData: wx_object.formData,
    //   success: wx_object.success,
    //   fail: wx_object.fail,
    //   complete: wx_object.complete
    // })
    return tt.uploadFile(wx_object)
  }

  //
  static connectSocket(wx_object) {
    return new WxSocketTask(tt.connectSocket(wx_object))
  }

  static onSocketError(wx_object) {
    return tt.onSocketError(wx_object)
  }

  static onSocketMessage(wx_object) {
    return tt.onSocketMessage(wx_object)
  }

  static sendSocketMessage(wx_object) {
    return tt.sendSocketMessage(wx_object)
  }

  static closeSocket(wx_object) {
    return tt.closeSocket(wx_object)
  }

  static offLocalServiceResolveFail(wx_object) {
    return tt.offLocalServiceResolveFail(wx_object)
  }

  static onLocalServiceResolveFail(wx_object) {
    return tt.onLocalServiceResolveFail(wx_object)
  }

  static offLocalServiceDiscoveryStop(wx_object) {
    return tt.offLocalServiceDiscoveryStop(wx_object)
  }

  static onLocalServiceDiscoveryStop(wx_object) {
    return tt.onLocalServiceDiscoveryStop(wx_object)
  }

  static offLocalServiceLost(wx_object) {
    return tt.offLocalServiceLost(wx_object)
  }

  static onLocalServiceLost(wx_object) {
    return tt.onLocalServiceLost(wx_object)
  }

  static offLocalServiceFound(wx_object) {
    return tt.offLocalServiceFound(wx_object)
  }

  static onLocalServiceFound(wx_object) {
    return tt.onLocalServiceFound(wx_object)
  }

  static stopLocalServiceDiscovery() {
    /* return tt.stopLocalServiceDiscovery(wx_object) */
  }

  static startLocalServiceDiscovery() {
    /*   return tt.startLocalServiceDiscovery(wx_object) */
  }


  // /////// Open Interface //////////
  static _checkSession() {
    const now = new Date().getTime()
    return getApp().onekit._jscode && getApp().onekit._login && now <= getApp().onekit._login + 1000 * 60 * 60 * 24 * 3
  }

  static checkSession(wx_object) {
    if (wx._checkSession()) {
      if (wx_object.success) {
        wx_object.success()
      }
      if (wx_object.complete) {
        wx_object.complete()
      }
    } else {
      if (wx_object.fail) {
        wx_object.fail()
      }
      if (wx_object.complete) {
        wx_object.complete()
      }
    }
  }

  static login(wx_object) {
    if (!wx_object) {
      tt.login(wx_object)
      return
    }
    const tt_object = {}
    tt_object.success = (res) => {
      getApp().onekit._jscode = res.code
      getApp().onekit._login = new Date().getTime()
      const result = {
        code: res.code
      }
      if (wx_object.success) {
        wx_object.success(result)
      }
      if (wx_object.complete) {
        wx_object.complete(result)
      }
    }
    tt_object.fail = (res) => {
      if (wx_object.fail) {
        wx_object.fail(res)
      }
      if (wx_object.complete) {
        wx_object.complete(res)
      }
    }
    /* console.log(wx._checkSession())
    if (wx._checkSession()) {
      tt_object.success({
        code: getApp().onekit._jscode
      })
    } else { */
    tt.login(tt_object)
    // }
  }


  static getUserInfo(wx_object) {
    wx.login({
      success: (res) => {
        const jscode = res.code
        const withCredentials = wx_object.withCredentials === true
        tt.getUserInfo({
          withCredentials,
          success(res) {
            console.log(res)
            const url = getApp().onekit.server + 'userinfo'
            tt.request({
              url,
              header: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              method: 'POST',
              data: {
                withCredentials,
                data: JSON.stringify(res),
                js_code: jscode
              },
              success(res) {
                if (wx_object.success) {
                  res.detail = res.data
                  wx_object.success(res.data)
                }
                if (wx_object.complete) {
                  wx_object.complete(res)
                }
              },
              fail(res) {
                console.log(res)
              }
            })
          }
        })
      }
    })
  }

  static getOpenData(wx_object) {
    if (!getApp().onekit._opendataCallbacks) {
      getApp().onekit._opendataCallbacks = []
    }

    function success(data) {
      const opendata = data.opendata
      getApp().onekit._opendata = opendata
      for (let cb = 0; cb < getApp().onekit._opendataCallbacks.length; cb++) {
        getApp().onekit._opendataCallbacks[cb](opendata)
      }
      if (wx_object.success) {
        wx_object.success(opendata)
      }
      if (wx_object.complete) {
        wx_object.complete(opendata)
      }
    }
    const opendata = getApp().onekit._opendata
    if (opendata) {
      if (Object.keys(opendata) > 0) {
        wx_object.success(opendata)
      } else if (wx_object.success) {
        getApp().onekit._opendataCallbacks.push(wx_object.success)
      }
      return
    }
    getApp().onekit._opendata = {}
    wx.login({
      success(res) {
        const jscode = res.code
        tt.getUserInfo({
          withCredentials: true,
          success(res) {
            const url = getApp().onekit.server + 'opendata'
            tt.request({
              url,
              header: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              method: 'POST',
              data: {
                data: JSON.stringify(res),
                js_code: jscode
              },
              success(res) {
                success(res.data)
              },
              fail(res) {
                console.log(res)
              }
            })
          }
        })
      }
    })
  }

  static _getPhoneNumber(data, callback) {
    wx.login({
      success: (res) => {
        const jscode = res.code
        const url = getApp().onekit.server + 'phonenumber'
        console.log(data, jscode)
        tt.request({
          url,
          header: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          data: {
            data: JSON.stringify(data),
            js_code: jscode
          },
          success(res) {
            const data = res.data
            callback(data)
          },
          fail(res) {
            console.log(res.data)
          }
        })
      }
    })
  }

  static getPhoneNumber(wx_object) {
    getApp().onekit._bindgetphonenumber = (data) => {
      this._getPhoneNumber(data, (res) => {
        if (wx_object.success) {
          wx_object.success(res)
        }
        if (wx_object.complete) {
          wx_object.complete(res)
        }
      })
    }
    tt.navigateTo({
      url: 'page/getphonenumber'
    })
  }

  static navigateToMiniProgram(wx_object) {
    return tt.navigateToMiniProgram(wx_object)
  }

  static navigateBackMiniProgram(wx_object) {
    return tt.navigateBackMiniProgram(wx_object)
  }

  static getAccountInfoSync(wx_object) {
    return tt.getAccountInfoSync(wx_object)
  }

  static reportMonitor() {
    /* return tt.reportMonitor(wx_object) */
  }

  static reportAnalytics(wx_object, eventName) {
    return tt.reportAnalytics(wx_object, eventName)
  }

  static requestPayment(wx_object) {
    const url = getApp().onekit.server + '/wx.requestPayment'
    console.log(url, wx_object)
    delete wx_object.success
    delete wx_object.fail
    delete wx_object.complete
    tt.request({
      url,
      data: wx_object,
      header: {},
      method: 'POST',
      success(res) {
        console.log(res)
        const tt_object = {
          orderInfo: res.data,
          service: 3,
          _debug: 1,
          success: wx_object.success,
          fail: wx_object.fail,
          complete: wx_object.complete
        }
        return tt.pay(tt_object)
      }
    })
  }

  static authorize(wx_object) {
    return tt.authorize(wx_object)
  }

  static openSetting(wx_object) {
    return tt.openSetting(wx_object)
  }

  static getSetting(wx_object) {
    return tt.getSetting(wx_object)
  }

  static chooseAddress(wx_object) {
    return tt.chooseAddress(wx_object)
  }

  static openCard(wx_object) {
    return tt.openCard(wx_object)
  }

  static addCard(wx_object) {
    return tt.addCard(wx_object)
  }

  static chooseInvoiceTitle(wx_object) {
    return tt.chooseInvoiceTitle(wx_object)
  }

  static chooseInvoice(wx_object) {
    return tt.chooseInvoice(wx_object)
  }

  static startSoterAuthentication(wx_object) {
    return tt.startSoterAuthentication(wx_object)
  }

  static checkIsSupportSoterAuthentication(wx_object) {
    return tt.checkIsSupportSoterAuthentication(wx_object)
  }

  static checkIsSoterEnrolledInDevice(wx_object) {
    return tt.checkIsSoterEnrolledInDevice(wx_object)
  }

  static getWeRunData(wx_object) {
    return tt.getWeRunData(wx_object)
  }


  // //////// Router //////////////
  static navigateBack(wx_object) {
    return tt.navigateBack(wx_object)
  }

  static switchTab(wx_object) {
    return tt.switchTab(wx_object)
  }

  static navigateTo(wx_object) {
    return tt.navigateTo(wx_object)
  }

  static reLaunch(wx_object) {
    return tt.reLaunch(wx_object)
  }

  static redirectTo(wx_object) {
    return tt.redirectTo(wx_object)
  }

  // /////////// Share /////////////
  static updateShareMenu(wx_object) {
    return tt.updateShareMenu(wx_object)
  }

  static showShareMenu(wx_object) {
    return tt.showShareMenu(wx_object)
  }

  static hideShareMenu(wx_object) {
    return tt.hideShareMenu(wx_object)
  }

  static getShareInfo(wx_object) {
    return tt.getShareInfo(wx_object)
  }

  // ///////////// Storage //////////////
  static getStorageInfoSync(wx_object) {
    return tt.getStorageInfoSync(wx_object)
  }

  static getStorageInfo(wx_object) {
    return tt.getStorageInfo(wx_object)
  }

  static clearStorageSync(wx_object) {
    return tt.clearStorageSync(wx_object)
  }

  static clearStorage(wx_object) {
    return tt.clearStorage(wx_object)
  }

  static removeStorageSync(wx_object) {
    return tt.removeStorageSync(wx_object)
  }

  static removeStorage(wx_object) {
    return tt.removeStorage(wx_object)
  }

  static setStorageSync(key, value) {
    return tt.setStorageSync({
      key,
      data: value
    })
  }

  static setStorage(wx_object) {
    return tt.setStorage(wx_object)
  }

  static getStorageSync(key) {
    return tt.getStorageSync(key)
  }

  static getStorage(wx_object) {
    return tt.getStorage(wx_object)
  }

  // //////////// UI ////////////////
  static showActionSheet(wx_object) {
    return tt.showActionSheet(wx_object)
  }


  // static redirectTo(wx_object) { return tt.redirectTo(wx_object) }
  // static redirectTo(wx_object) { return tt.redirectTo(wx_object) }
  static hideLoading(wx_object) {
    return tt.hideLoading(wx_object)
  }

  static showLoading(wx_object) {
    return tt.showLoading(wx_object)
  }

  static hideToast(wx_object) {
    return tt.hideToast(wx_object)
  }

  static showToast(wx_object) {
    return tt.showToast(wx_object)
  }

  static showModal(wx_object) {
    return tt.showModal(wx_object)
  }

  static setNavigationBarColor(wx_object) {
    return tt.setNavigationBarColor(wx_object)
  }

  static hideNavigationBarLoading(wx_object) {
    return tt.hideNavigationBarLoading(wx_object)
  }

  static showNavigationBarLoading(wx_object) {
    return tt.showNavigationBarLoading(wx_object)
  }

  static setNavigationBarTitle(wx_object) {
    return tt.setNavigationBarTitle(wx_object)
  }

  static setBackgroundTextStyle(wx_object) {
    return tt.setBackgroundTextStyle(wx_object)
  }

  static setBackgroundColor() {
    /* return tt.setBackgroundColor(wx_object) */
  }

  static setTabBarItem(wx_object) {
    return tt.setTabBarItem(wx_object)
  }

  static setTabBarStyle(wx_object) {
    return tt.setTabBarStyle(wx_object)
  }

  static hideTabBar(wx_object) {
    return tt.hideTabBar(wx_object)
  }

  static showTabBar(wx_object) {
    return tt.showTabBar(wx_object)
  }

  static hideTabBarRedDot(wx_object) {
    return tt.hideTabBarRedDot(wx_object)
  }

  static showTabBarRedDot(wx_object) {
    return tt.showTabBarRedDot(wx_object)
  }

  static removeTabBarBadge(wx_object) {
    return tt.removeTabBarBadge(wx_object)
  }

  static setTabBarBadge(wx_object) {
    return tt.setTabBarBadge(wx_object)
  }

  static loadFontFace() {
    /* return tt.loadFontFace(wx_object) */
  }

  static stopPullDownRefresh(wx_object) {
    return tt.stopPullDownRefresh(wx_object)
  }

  static startPullDownRefresh(wx_object) {
    return tt.startPullDownRefresh(wx_object)
  }

  static pageScrollTo(wx_object) {
    return tt.pageScrollTo(wx_object)
  }

  static setTopBarText(wx_object) {
    return tt.setTopBarText(wx_object)
  }

  static nextTick(wx_object) {
    return tt.nextTick(wx_object)
  }

  static getMenuButtonBoundingClientRect(wx_object) {
    return tt.getMenuButtonBoundingClientRect(wx_object)
  }

  static offWindowResize(wx_object) {
    return tt.offWindowResize(wx_object)
  }

  static onWindowResize(wx_object) {
    return tt.onWindowResize(wx_object)
  }

  // //////////// Worker ///////////////
  static createWorker(path) {
    return new WORKER(path)
  }

  // //////////// WXML ///////////////
  static createSelectorQuery(wx_object) {
    return tt.createSelectorQuery(wx_object)
  }

  static createIntersectionObserver(wx_object) {
    return tt.createIntersectionObserver(wx_object)
  }

  // ///////////////////////////////////
  static hideKeyboard(wx_object) {
    return tt.hideKeyboard(wx_object)
  }

  static setBackgroundFetchToken() {

  }

  // ////////////////////////////////////
  // /////////// cloud ////////////////
  static get cloud() {
    return new WxCloud()
  }
}
