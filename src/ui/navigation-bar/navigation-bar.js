/* eslint-disable no-bitwise */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */

Component({
  data: { },
  properties: {
    title: {
      type: String
    },
    loading: {
      type: Boolean | String,
      value: false
    },
    frontColor: {
      type: String
    },
    backgroundColor: {
      type: String
    },
    colorAnimationDuration: {
      type: Number,
      value: 0
    },
    colorAnimationTimingFunc: {
      type: String,
      value: 'linear'
    }
  },
  methods: { },
  lifetimes: {
    attached() {
      const title = this.data.title
      const loading = this.data.loading
      const frontColor = this.data.frontColor
      const backgroundColor = this.data.backgroundColor
      const colorAnimationDuration = this.data.colorAnimationDuration
      const colorAnimationTimingFunc = this.data.colorAnimationTimingFunc

      if (title) {
        tt.setNavigationBarTitle({
          title
        })
      }

      if (loading || loading === 'true') {
        tt.showNavigationBarLoading()
      } else {
        tt.hideNavigationBarLoading()
      }

      if (frontColor || backgroundColor) {
        tt.setNavigationBarColor({
          frontColor,
          backgroundColor
        })
      }
    }
  }
})
