Component({
  mixins: [],
  data: {},
  properties: {
    indicatorDots: false,
    indicatorColor: 'rgba(0, 0, 0, .3)',
    indicatorActiveColor: '#000000',
    autoplay: false,
    current: 0,
    interval: 5000,
    duration: 500,
    circular: false,
    vertical: false,
    previousMargin: '0px',
    nextMargin: '0px',
  },
  didMount() {},
  didUpdate() {},
  didUnmount() {},
  methods: {
    swiper_Change(e) {
      if (this.properties.change) {
        this.properties.change(e.details)
      }
    },
    swiper_Transition(e) {
      if (this.properties.transition) {
        this.properties.transition(e.details)
      }
    },
    swiper_AnimationEnd(e) {
      if (this.properties.animationfinish) {
        this.properties.animationfinish(e.details)
      }
    }
  },
})
