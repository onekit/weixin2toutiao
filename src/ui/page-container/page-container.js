/* eslint-disable no-bitwise */
/* eslint-disable no-console */
Component({
  data: {
    overlay_color: 'rgb(0, 0, 0, 0.7)',
    keyframes: null,
    overlay_keyframes: null
  },
  properties: {
    show: {
      type: Boolean,
      value: false,
      observer(newVal, oldVal) {
        console.log('observer新值:', newVal, '旧值:', oldVal)
      }
    },
    duration: {
      type: Number,
      value: 300
    },
    zIndex: {
      type: Number,
      value: 100
    },
    overlay: {
      type: Boolean | String,
      value: true
    },
    position: {
      type: String,
      value: 'bottom'
    },
    round: {
      type: Boolean,
      value: false
    },
    closeOnSlideDown: {
      type: Boolean,
      value: false
    },
    overlayStyle: {
      type: String
    },
    customStyle: {
      type: String
    }
  },
  methods: {
    touchStart(e) {
      this.startY = e.touches[0].pageY
    },
    touchMove(e) {
      this.endY = e.touches[0].pageY
    },
    touchEnd() {
      if (this.data.closeOnSlideDown && this.endY - this.startY >= 50) {
        this.setData({
          overlay_keyframes: 'overlay'
        })
        switch (this.data.position) {
          case 'top':
            this.setData({
              keyframes: 'top',
            })
            break
          case 'bottom':
            this.setData({
              keyframes: 'bottom'
            })
            break
          case 'center':
            this.setData({
              keyframes: 'cneter'
            })
            break
          case 'right':
            this.setData({
              keyframes: 'right'
            })
            break
          default:
            break
        }

        // setTimeout(() => {
        //   this.setData({
        //     show: false,
        //     keyframes: null,
        //     overlay_keyframes: null
        //   })
        // }, this.data.duration)
        this.setData({
          show: false
        })
      }
      console.log('touchEnd', this.data.show)
    },
    click_overlay() {
      this.triggerEvent('clickoverlay', this)
    }
  },
  lifetimes: {
    attached() {
      console.log('attached', this.data.show)
      const data = {}
      switch (this.data.position) {
        case 'bottom':
          data.set_position = 'bottom'
          if (this.data.round) {
            data.round_style = 'border-radius:20px 20px 0 0;overflow:hidden'
          }
          break
        case 'top':
          data.set_position = 'top'
          if (this.data.round) {
            data.round_style = 'border-radius:0 0 20px 20px;overflow:hidden'
          }
          break
        case 'center':
          data.set_position = 'top'
          data.overlay_color = '#FFFFFF'
          break
        case 'right':
          data.set_position = 'top'
          data.overlay_color = '#FFFFFF'
          break
        default:
          break
      }

      if (this.data.overlay === 'false') {
        data.overlay_color = '#FFFFFF'
      }
      this.setData(data)
    }
  },
  observers: {
    show(value) {
      if (value) {
        this.triggerEvent('beforeenter', this)
        this.triggerEvent('enter', this)
        setTimeout(() => {
          this.triggerEvent('afterenter', this)
        }, this.data.duration)
      } else if (!value) {
        this.triggerEvent('beforeleave', this)
        this.triggerEvent('leave', this)
        setTimeout(() => {
          this.triggerEvent('afterleave', this)
        }, this.data.duration)
      }
    }
  }
})
