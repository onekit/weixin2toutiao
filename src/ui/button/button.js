/* eslint-disable no-console */
import wx from '../../wx'

Component({
  mixins: [],
  data: {
    hideContact: true
  },
  properties: {},
  methods: {
    contactBG_tap() {
      this.setData({
        hideContact: true
      })
    },
    button_tap(e) {
      const that = this
      console.log('button', this.properties)
      if (this.properties.openType) {
        switch (this.properties.openType) {
          case 'contact':
            this.setData({
              hideContact: false
            })
            break
          case 'share':
            wx.showShareMenu({
              success() {

              }
            })
            break
          case 'getUserInfo':
            wx.getUserInfo({
              success(res) {
                e.detail = res
                that.triggerEvent('Getuserinfo', e)
              }
            })

            break
          case 'getPhoneNumber':
            wx.getPhoneNumber({
              success(res) {
                e.detail = res
                that.triggerEvent('Getphonenumber', e)
              }
            })

            break
          case 'launchApp':
            break
          case 'openSetting':
            wx.openSetting({

            })
            break
          case 'feedback':
            break
          default:
            console.error(this.properties.openType)
            break
        }
      }
      this.triggerEvent('Tap', {})
    },
  },
})
