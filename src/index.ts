import App from './OnekitApp'
import Behavior from './OnekitBehavior'
import Component from './OnekitComponent'
import Page from './OnekitPage'
import wx from './wx'

export {
  App,
  Behavior,
  Component,
  Page,
  wx,
}
