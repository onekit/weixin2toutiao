import {wx} from '../../../../weixin2toutiao/index.js';
import {Page,Component} from '../../../../weixin2toutiao/index.js';
Page({
    "onShareAppMessage"(){
      return {
        "title":'input',
        "path":'page/component/pages/input/input'
};
    },
    "data":{
        "focus":false,
        "inputValue":''
},
    "bindKeyInput"(e){
      this.setData({
        "inputValue":e.detail.value
});
    },
    "bindReplaceInput"(e){
      const value = e.detail.value;
      let pos = e.detail.cursor;
      let left;
      if(pos !== - 1)
      {
        left = e.detail.value.slice(0,pos);
        pos = left.replace(/11/g,'2').length;
      };
      return {
        "value":value.replace(/11/g,'2'),
        "cursor":pos
};
    },
    "bindHideKeyboard"(e){
      if(e.detail.value == '123')
      {
        wx.hideKeyboard();
      };
    }
});

