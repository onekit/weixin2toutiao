import {wx} from '../../../weixin2toutiao/index.js';
import {Page,Component} from '../../../weixin2toutiao/index.js';
Page({
    "onShareAppMessage"(){
      return {
        "title":'WXML节点布局相交状态',
        "path":'packageAPI/pages/intersection-observer/intersection-observer'
};
    },
    "data":{
        "appear":false
},
    "onLoad"(){
      this._observer = wx.createIntersectionObserver(this);
      this._observer.relativeTo('.scroll-view').observe('.ball',(res)=>{
      console.log(res);
      this.setData({
          "appear":res.intersectionRatio > 0
});
    });
    },
    "onUnload"(){
      if(this._observer)
      this._observer.disconnect();
    }
});

