import {wx} from '../../../weixin2toutiao/index.js';
import {Page,Component} from '../../../weixin2toutiao/index.js';
Page({
    "onShareAppMessage"(){
      return {
        "title":'收货地址',
        "path":'packageAPI/pages/choose-address/choose-address'
};
    },
    "data":{
        "addressInfo":null
},
    "chooseAddress"(){
      wx.chooseAddress({
        "success":(res)=>{this.setData({
            "addressInfo":res
})},
        "fail"(err){
          console.log(err);
        }
});
    }
});

