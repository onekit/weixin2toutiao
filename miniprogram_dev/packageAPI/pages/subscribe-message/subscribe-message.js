import {wx} from '../../../weixin2toutiao/index.js';
import {Page,Component} from '../../../weixin2toutiao/index.js';
Page({
    "data":{},
    "requestSubscribeMessage"(){
      const self = this;
      wx.requestSubscribeMessage({
        "tmplIds":[
          'y1bXHAg_oDuvrQ3pHgcODcMPl-2hZHenWugsqdB2CXY'
        ],
        "success"(res){
          console.log(res);
          if(res.errMsg == 'requestSubscribeMessage:ok')
          {
            self.subscribeMessageSend();
          };
        },
        "complete"(res){
          console.log(res);
        }
});
    },
    "subscribeMessageSend"(){
      wx.cloud.callFunction({
        "name":'openapi',
        "data":{
            "action":'sendSubscribeMessage'
},
        "success":(res)=>{
          console.warn('[云函数] [openapi] templateMessage.send 调用成功：',res);
          wx.showModal({
              "title":'订阅成功',
              "content":'请返回微信主界面查看',
              "showCancel":false
});
        },
        "fail":(err)=>{
          wx.showToast({
              "icon":'none',
              "title":'调用失败'
});
          console.error('[云函数] [openapi] templateMessage.send 调用失败：',err);
        }
});
    },
    "onLoad"(options){
    },
    "onReady"(){
    },
    "onShow"(){
    },
    "onHide"(){
    },
    "onUnload"(){
    },
    "onPullDownRefresh"(){
    },
    "onReachBottom"(){
    },
    "onShareAppMessage"(){
      return {
        "title":'订阅消息',
        "path":'packageAPI/pages/subscribe-message/subscribe-message'
};
    }
});

