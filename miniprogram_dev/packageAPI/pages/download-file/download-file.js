import {wx} from '../../../weixin2toutiao/index.js';
import {Page,Component} from '../../../weixin2toutiao/index.js';
const demoImageFileId = require('../../../config').demoImageFileId;
Page({
    "onShareAppMessage"(){
      return {
        "title":'下载文件',
        "path":'packageAPI/pages/download-file/download-file'
};
    },
    "downloadImage"(){
      const self = this;
      wx.cloud.downloadFile({
        "fileID":demoImageFileId,
        "success":(res)=>{
          console.log('downloadFile success, res is',res);
          self.setData({
              "imageSrc":res.tempFilePath
});
        },
        "fail":console.error
});
    }
});

