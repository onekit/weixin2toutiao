import {wx} from '../../../weixin2toutiao/index.js';
import {Page,Component} from '../../../weixin2toutiao/index.js';
const app = getApp();
Page({
    "onShareAppMessage"(){
      return {
        "title":'微信登录',
        "path":'package/API/pages/login/login'
};
    },
    "onLoad"(){
      this.setData({
        "hasLogin":app.globalData.hasLogin
});
    },
    "data":{},
    "login"(){
      const that = this;
      wx.login({
        "success"(){
          app.globalData.hasLogin = true;
          that.setData({
            "hasLogin":true
});
        }
});
    }
});

