module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint-disable max-len */
/* eslint-disable camelcase */

var bd_USER_FOLDER = 'bdfile://store/';
var tt_USER_FOLDER = tt.env.USER_DATA_PATH + '/';

function current() {
  var pages = getCurrentPages();
  if (pages.length === 0) {
    return {};
  }
  return pages[pages.length - 1];
}

function currentUrl() {
  return current().route;
}

function new_bd_filePath(ext) {
  var randomString = Math.floor(Math.random() * (1 - 10000000) + 10000000);
  var bd_filePath = '' + bd_USER_FOLDER + randomString + ext;
  return bd_filePath;
}

function bd_filePath2tt_filePath(bd_filePath) {
  // eslint-disable-next-line no-undef
  if (!getApp().bdSavePath2ttStorePath) {
    return bd_filePath;
  }
  // eslint-disable-next-line no-undef
  var tt_storePath = getApp().bdSavePath2ttStorePath[bd_filePath];
  if (tt_storePath) {
    return tt_storePath;
  } else {
    var tt_filePath = bd_filePath.replace(bd_USER_FOLDER, tt_USER_FOLDER);
    return tt_filePath;
  }
}

function save_tt_storePath(bd_filePath, tt_storePath) {
  // eslint-disable-next-line no-undef
  if (!getApp().bdSavePath2ttStorePath) {
    // eslint-disable-next-line no-undef
    getApp().bdSavePath2ttStorePath = {};
  }
  // eslint-disable-next-line no-undef
  getApp().bdSavePath2ttStorePath[bd_filePath] = tt_storePath;
  // ///////////////////////
  // eslint-disable-next-line no-undef
  if (!getApp().ttStorePath2bdSavePath) {
    // eslint-disable-next-line no-undef
    getApp().ttStorePath2bdSavePath = {};
  }
  // eslint-disable-next-line no-undef
  getApp().ttStorePath2bdSavePath[tt_storePath] = bd_filePath;
}
module.exports = {
  current: current,
  currentUrl: currentUrl,
  save_tt_storePath: save_tt_storePath,
  new_bd_filePath: new_bd_filePath,
  bd_filePath2tt_filePath: bd_filePath2tt_filePath
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable no-undef */
/* eslint-disable prefer-spread */
/* eslint-disable no-console */
/* eslint-disable max-len */
/* eslint-disable camelcase */

// import VideoContext from './api/VideoContext'
// import LivePlayerContext from './api/LivePlayerContext'


var _CanvasContext = __webpack_require__(2);

var _CanvasContext2 = _interopRequireDefault(_CanvasContext);

var _WORKER = __webpack_require__(3);

var _WORKER2 = _interopRequireDefault(_WORKER);

var _wx = __webpack_require__(4);

var _wx2 = _interopRequireDefault(_wx);

var _onekit = __webpack_require__(0);

var _onekit2 = _interopRequireDefault(_onekit);

var _WxSocketTask = __webpack_require__(5);

var _WxSocketTask2 = _interopRequireDefault(_WxSocketTask);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var wx = function () {
  function wx() {
    _classCallCheck(this, wx);
  }

  // ///////////////// animation //////////////////////////
  wx.createAnimation = function createAnimation(wx_object) {
    return tt.createAnimation(wx_object);
  };

  // /////////////// basic ////////////////////////////////


  wx.canIUse = function canIUse() {
    return true;
  };

  wx._getSystemInfo = function _getSystemInfo(wx_object) {
    return tt._getSystemInfo(wx_object);
  };

  wx.getSystemInfo = function getSystemInfo(wx_object) {
    return tt.getSystemInfo(wx_object);
  };

  wx.getSystemInfoSync = function getSystemInfoSync(wx_object) {
    return tt.getSystemInfoSync(wx_object);
  };

  wx.base64ToArrayBuffer = function base64ToArrayBuffer(base64) {
    return tt.base64ToArrayBuffer(base64);
  };

  wx.arrayBufferToBase64 = function arrayBufferToBase64(arrayBuffer) {
    return tt.arrayBufferToBase64(arrayBuffer);
  };

  wx.getEnterOptionsSync = function getEnterOptionsSync(wx_object) {
    return tt.getEnterOptionsSync(wx_object);
  };

  wx.getUpdateManager = function getUpdateManager(wx_object) {
    return tt.getUpdateManager(wx_object);
  };

  wx.getLaunchOptionsSync = function getLaunchOptionsSync(wx_object) {
    return tt.getLaunchOptionsSync(wx_object);
  };

  wx.exitMiniProgram = function exitMiniProgram(wx_object) {
    return tt.exitMiniProgram(wx_object);
  };

  wx.offPageNotFound = function offPageNotFound(wx_object) {
    return tt.offPageNotFound(wx_object);
  };

  wx.onPageNotFound = function onPageNotFound(wx_object) {
    return tt.onPageNotFound(wx_object);
  };

  wx.offError = function offError(wx_object) {
    return tt.offError(wx_object);
  };

  wx.onError = function onError(wx_object) {
    return tt.onError(wx_object);
  };

  wx.offAppShow = function offAppShow(wx_object) {
    return tt.offAppShow(wx_object);
  };

  wx.onAppShow = function onAppShow(wx_object) {
    return tt.onAppShow(wx_object);
  };

  wx.offAppHide = function offAppHide(wx_object) {
    return tt.offAppHide(wx_object);
  };

  wx.onAppHide = function onAppHide(wx_object) {
    return tt.onAppHide(wx_object);
  };

  wx.setEnableDebug = function setEnableDebug(wx_object) {
    return tt.setEnableDebug(wx_object);
  };

  wx.getLogManager = function getLogManager(wx_object) {
    return tt.getLogManager(wx_object);
  };

  // ///////////////// Canvas ///////////////////
  // eslint-disable-next-line complexity


  wx.drawCanvas = function drawCanvas(wx_object) {
    var canvasId = wx_object.canvasId;
    var actions = wx_object.actions;
    var canvasContext = tt.createCanvasContext(canvasId);
    for (var _iterator = actions, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
      var _ref;

      if (_isArray) {
        if (_i >= _iterator.length) break;
        _ref = _iterator[_i++];
      } else {
        _i = _iterator.next();
        if (_i.done) break;
        _ref = _i.value;
      }

      var action = _ref;

      var data = action.data;
      switch (action.method) {
        case 'save':
          canvasContext.save();
          break;
        case 'restore':
          canvasContext.restore();
          break;
        case 'setFillStyle':
          canvasContext.setFillStyle(_onekit2.default.color.array2str(data[1]));
          break;
        case 'setStrokeStyle':
          canvasContext.setStrokeStyle(_onekit2.default.color.array2str(data[1]));
          break;
        case 'setFontSize':
          canvasContext.setFontSize(data[0]);
          break;
        case 'setGlobalAlpha':
          canvasContext.setGlobalAlpha(data[0]);
          break;
        case 'setShadow':
          canvasContext.setShadow(data[0], data[1], data[2], _onekit2.default.color.array2str(data[3]));

          break;
        case 'drawImage':
          canvasContext.drawImage.apply(canvasContext, data);
          break;
        case 'fillText':
          canvasContext.fillText.apply(canvasContext, data);
          break;
        case 'setLineCap':
          canvasContext.setLineCap(data[0]);
          break;
        case 'setLineJoin':
          canvasContext.setLineJoin(data[0]);
          break;
        case 'setLineWidth':
          canvasContext.setLineWidth(data[0]);
          break;
        case 'setMiterLimit':
          canvasContext.setMiterLimit(data[0]);
          break;
        case 'rotate':
          canvasContext.rotate(data[0]);
          break;
        case 'scale':
          canvasContext.scale(data[0], data[1]);
          break;
        case 'translate':
          canvasContext.translate(data[0], data[1]);
          break;
        case 'strokePath':
          canvasContext.beginPath();
          for (var _iterator2 = data, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
            var _ref2;

            if (_isArray2) {
              if (_i2 >= _iterator2.length) break;
              _ref2 = _iterator2[_i2++];
            } else {
              _i2 = _iterator2.next();
              if (_i2.done) break;
              _ref2 = _i2.value;
            }

            var dat = _ref2;

            var dt = dat.data;
            switch (dat.method) {
              case 'rect':
                canvasContext.strokeRect(dt[0], dt[1], dt[2], dt[3]);
                break;
              case 'moveTo':
                canvasContext.moveTo(dt[0], dt[1]);
                break;
              case 'lineTo':
                canvasContext.lineTo(dt[0], dt[1]);
                break;
              case 'closePath':
                canvasContext.closePath();
                break;
              case 'arc':
                canvasContext.arc.apply(canvasContext, dt);
                break;
              case 'quadraticCurveTo':
                canvasContext.quadraticCurveTo.apply(canvasContext, dt);
                break;
              case 'bezierCurveTo':
                canvasContext.bezierCurveTo.apply(canvasContext, dt);
                break;

              default:
                console.log('[drawCanvas-strokePath]', dat.method);
                break;
            }
          }
          canvasContext.stroke();
          break;
        case 'fillPath':
          for (var _iterator3 = data, _isArray3 = Array.isArray(_iterator3), _i3 = 0, _iterator3 = _isArray3 ? _iterator3 : _iterator3[Symbol.iterator]();;) {
            var _ref3;

            if (_isArray3) {
              if (_i3 >= _iterator3.length) break;
              _ref3 = _iterator3[_i3++];
            } else {
              _i3 = _iterator3.next();
              if (_i3.done) break;
              _ref3 = _i3.value;
            }

            var _dat = _ref3;

            var _dt = _dat.data;
            switch (_dat.method) {
              case 'rect':
                canvasContext.fillRect(_dt[0], _dt[1], _dt[2], _dt[3]);
                break;
              case 'arc':
                canvasContext.arc.apply(canvasContext, _dt);
                break;
              default:
                console.log('[drawCanvas-fillPath]', _dat.method);
                break;
            }
          }
          canvasContext.fill();
          break;
        case 'clearRect':
          canvasContext.clearRect(data[0], data[1], data[2], data[3]);
          break;
        default:
          console.log('[drawCanvas]', action.method);
          break;
      }
    }
    canvasContext.draw();
  };

  wx.createContext = function createContext() {
    var context = new _CanvasContext2.default();
    return context;
  };

  wx.createCanvasContext = function createCanvasContext(wx_object) {
    return tt.createCanvasContext(wx_object);
  };

  wx.canvasToTempFilePath = function canvasToTempFilePath(wx_object) {
    return tt.canvasToTempFilePath(wx_object);
  };

  wx.canvasPutImageData = function canvasPutImageData(wx_object) {
    return tt.canvasPutImageData(wx_object);
  };

  wx.canvasGetImageData = function canvasGetImageData(wx_object) {
    return tt.canvasGetImageData(wx_object);
  };

  // //////////// Device //////////////////


  wx.onBeaconServiceChange = function onBeaconServiceChange(wx_object) {
    return tt.onBeaconServiceChange(wx_object);
  };

  wx.onBeaconUpdate = function onBeaconUpdate(wx_object) {
    return tt.onBeaconUpdate(wx_object);
  };

  wx.getBeacons = function getBeacons(wx_object) {
    return tt.getBeacons(wx_object);
  };

  wx.stopBeaconDiscovery = function stopBeaconDiscovery() {
    /* return tt.stopBeaconDiscovery(wx_object); */
  };

  wx.startBeaconDiscovery = function startBeaconDiscovery() {
    /* return tt.startBeaconDiscovery(wx_object); */
  };

  wx.stopWifi = function stopWifi() {
    /* return tt.stopWifi(wx_object); */
  };

  wx.startWifi = function startWifi() {
    /* return tt.startWifi(wx_object); */
  };

  wx.setWifiList = function setWifiList(wx_object) {
    return tt.setWifiList(wx_object);
  };

  wx.createMediaContainer = function createMediaContainer() {
    return console.warn('.createMediaContainer is not suport');
  };

  wx.onWifiConnected = function onWifiConnected(wx_callback) {
    return tt.onWifiConnected(wx_callback);
  };

  wx.onGetWifiList = function onGetWifiList(wx_callback) {
    return tt.onGetWifiList(wx_callback);
  };

  wx.offGetWifiList = function offGetWifiList(wx_callback) {
    return tt.offGetWifiList(wx_callback);
  };

  wx.getWifiList = function getWifiList(wx_object) {
    return tt.getWifiList(wx_object);
  };

  wx.getConnectedWifi = function getConnectedWifi(wx_object) {
    return tt.getConnectedWifi(wx_object);
  };

  wx.connectWifi = function connectWifi(wx_object) {
    return tt.connectWifi(wx_object);
  };

  //


  wx.onAccelerometerChange = function onAccelerometerChange(callback) {
    return tt.onAccelerometerChange(callback);
  };

  wx.stopAccelerometer = function stopAccelerometer(wx_object) {
    return tt.stopAccelerometer(wx_object);
  };

  wx.startAccelerometer = function startAccelerometer(wx_object) {
    return tt.startAccelerometer(wx_object);
  };

  wx.getBatteryInfoSync = function getBatteryInfoSync(wx_object) {
    return tt.getBatteryInfoSync(wx_object);
  };

  wx.getBatteryInfo = function getBatteryInfo() {
    return console.warn('getBatteryInfo is not support');
  };

  //


  wx.getClipboardData = function getClipboardData(wx_object) {
    return tt.getClipboardData(wx_object);
  };

  wx.setClipboardData = function setClipboardData(wx_object) {
    return tt.setClipboardData(wx_object);
  };

  wx.onCompassChange = function onCompassChange(callback) {
    return tt.onCompassChange(callback);
  };

  wx.stopCompass = function stopCompass(wx_object) {
    return tt.stopCompass(wx_object);
  };

  wx.onBLEPeripheralConnectionStateChanged = function onBLEPeripheralConnectionStateChanged() {
    return console.warn('onBLEPeripheralConnectionStateChanged is not suport');
  };

  wx.startCompass = function startCompass(wx_object) {
    return tt.startCompass(wx_object);
  };

  wx.addPhoneContact = function addPhoneContact() {
    /* return tt.addPhoneContact(wx_object); */
  };

  wx.onGyroscopeChange = function onGyroscopeChange(callback) {
    return tt.onGyroscopeChange(callback);
  };

  wx.stopGyroscope = function stopGyroscope(wx_object) {
    return tt.stopGyroscope(wx_object);
  };

  wx.startGyroscope = function startGyroscope(wx_object) {
    return tt.startGyroscope(wx_object);
  };

  //


  wx.onDeviceMotionChange = function onDeviceMotionChange(wx_object) {
    return tt.onDeviceMotionChange(wx_object);
  };

  wx.onSocketonOpen = function onSocketonOpen(wx_callback) {
    getApp().onekit_onSocketonOpen = wx_callback;
  };

  wx.onSocketonMessage = function onSocketonMessage(wx_callback) {
    getApp().onekit_onSocketonMessage = wx_callback;
  };

  wx.createUDPSocket = function createUDPSocket() {
    return console.warn('createUDPSocket is not suport');
  };

  wx.onSocketonError = function onSocketonError(wx_callback) {
    getApp().onekit_onSocketonError = wx_callback;
  };

  wx.onSocketClose = function onSocketClose(wx_callback) {
    getApp().onekit_onSocketClose = wx_callback;
  };

  wx.onSocketonClose = function onSocketonClose(wx_callback) {
    getApp().onekit_onSocketonClose = wx_callback;
  };

  wx.onSocketSend = function onSocketSend(wx_callback) {
    getApp().onekit_onSocketSend = wx_callback;
  };

  wx.stopDeviceMotionListening = function stopDeviceMotionListening(wx_object) {
    return tt.stopDeviceMotionListening(wx_object);
  };

  wx.startDeviceMotionListening = function startDeviceMotionListening(wx_object) {
    return tt.startDeviceMotionListening(wx_object);
  };

  //


  wx.getNetworkType = function getNetworkType(wx_object) {
    return tt.getNetworkType(wx_object);
  };

  wx._network = function _network(res) {
    return tt._network(res);
  };

  wx.onNetworkStatusChange = function onNetworkStatusChange(callack) {
    return tt.onNetworkStatusChange(callack);
  };

  //


  wx.makePhoneCall = function makePhoneCall(wx_object) {
    return tt.makePhoneCall(wx_object);
  };

  wx.scanCode = function scanCode(wx_object) {
    return tt.scanCode(wx_object);
  };

  //


  wx.vibrateLong = function vibrateLong(wx_object) {
    return tt.vibrateLong(wx_object);
  };

  wx.vibrateShort = function vibrateShort(wx_object) {
    return tt.vibrateShort(wx_object);
  };

  //


  wx.onMemoryWarning = function onMemoryWarning(wx_object) {
    return tt.onMemoryWarning(wx_object);
  };

  //


  wx.writeBLECharacteristicValue = function writeBLECharacteristicValue(wx_object) {
    return tt.writeBLECharacteristicValue(wx_object);
  };

  wx.readBLECharacteristicValue = function readBLECharacteristicValue(wx_object) {
    return tt.readBLECharacteristicValue(wx_object);
  };

  wx.onBLEConnectionStateChange = function onBLEConnectionStateChange(wx_object) {
    return tt.onBLEConnectionStateChange(wx_object);
  };

  wx.onBLECharacteristicValueChange = function onBLECharacteristicValueChange(wx_object) {
    return tt.onBLECharacteristicValueChange(wx_object);
  };

  wx.notifyBLECharacteristicValueChange = function notifyBLECharacteristicValueChange(wx_object) {
    return tt.notifyBLECharacteristicValueChange(wx_object);
  };

  wx.getBLEDeviceServices = function getBLEDeviceServices(wx_object) {
    return tt.getBLEDeviceServices(wx_object);
  };

  wx.getBLEDeviceCharacteristics = function getBLEDeviceCharacteristics(wx_object) {
    return tt.getBLEDeviceCharacteristics(wx_object);
  };

  wx.createBLEConnection = function createBLEConnection(wx_object) {
    return tt.createBLEConnection(wx_object);
  };

  wx.closeBLEConnection = function closeBLEConnection(wx_object) {
    return tt.closeBLEConnection(wx_object);
  };

  //


  wx.stopBluetoothDevicesDiscovery = function stopBluetoothDevicesDiscovery() {
    /* return tt.stopBluetoothDevicesDiscovery(wx_object); */
  };

  wx.startBluetoothDevicesDiscovery = function startBluetoothDevicesDiscovery() {
    /* return tt.startBluetoothDevicesDiscovery(wx_object) */
  };

  wx.openBluetoothAdapter = function openBluetoothAdapter() {
    /* return tt.openBluetoothAdapter(wx_object); */
  };

  wx.onBluetoothDeviceFound = function onBluetoothDeviceFound() {
    /* return tt.onBluetoothDeviceFound(wx_object) */
  };

  wx.onBluetoothAdapterStateChange = function onBluetoothAdapterStateChange(wx_object) {
    return tt.onBluetoothAdapterStateChange(wx_object);
  };

  wx.getConnectedBluetoothDevices = function getConnectedBluetoothDevices(wx_object) {
    return tt.getConnectedBluetoothDevices(wx_object);
  };

  wx.getBluetoothDevices = function getBluetoothDevices(wx_object) {
    return tt.getBluetoothDevices(wx_object);
  };

  wx.getBluetoothAdapterState = function getBluetoothAdapterState() {
    /* return tt.getBluetoothAdapterState(wx_object) */
  };

  wx.closeBluetoothAdapter = function closeBluetoothAdapter() {}
  /* return tt.closeBluetoothAdapter(wx_object); */


  //
  ;

  wx.stopHCE = function stopHCE(wx_object) {
    return tt.stopHCE(wx_object);
  };

  wx.startHCE = function startHCE(wx_object) {
    return tt.startHCE(wx_object);
  };

  wx.sendHCEMessage = function sendHCEMessage(wx_object) {
    return tt.sendHCEMessage(wx_object);
  };

  wx.onHCEMessage = function onHCEMessage(wx_object) {
    return tt.onHCEMessage(wx_object);
  };

  wx.getHCEState = function getHCEState(wx_object) {
    return tt.getHCEState(wx_object);
  };

  //


  wx.setScreenBrightness = function setScreenBrightness(wx_object) {
    return tt.setScreenBrightness(wx_object);
  };

  wx.setKeepScreenOn = function setKeepScreenOn(wx_object) {
    return tt.setKeepScreenOn(wx_object);
  };

  wx.onUserCaptureScreen = function onUserCaptureScreen(wx_object) {
    return tt.onUserCaptureScreen(wx_object);
  };

  wx.offUserCaptureScreen = function offUserCaptureScreen(wx_object) {
    return tt.offUserCaptureScreen(wx_object);
  };

  wx.getScreenBrightness = function getScreenBrightness(wx_object) {
    return tt.getScreenBrightness(wx_object);
  };

  // ///////////////// Ext //////////////


  wx.getExtConfigSync = function getExtConfigSync(wx_object) {
    return tt.getExtConfigSync(wx_object);
  };

  wx.getExtConfig = function getExtConfig(wx_object) {
    return tt.getExtConfig(wx_object);
  };

  // ////////////////// File //////////


  wx.getFileSystemManager = function getFileSystemManager(wx_object) {
    return tt.getFileSystemManager(wx_object);
  };

  wx.getFileInfo = function getFileInfo(wx_object) {
    return tt.getFileInfo(wx_object);
  };

  wx.removeSavedFile = function removeSavedFile(wx_object) {
    return tt.removeSavedFile(wx_object);
  };

  wx.getSavedFileInfo = function getSavedFileInfo(wx_object) {
    return tt.getSavedFileInfo(wx_object);
  };

  wx.getSavedFileList = function getSavedFileList(wx_object) {
    return tt.getSavedFileList(wx_object);
  };

  wx.openDocument = function openDocument(wx_object) {
    return tt.openDocument(wx_object);
  };

  wx.saveFile = function saveFile(wx_object) {
    return tt.saveFile(wx_object);
  };

  // ////////// Location ///////////////


  wx.openLocation = function openLocation(wx_object) {
    return tt.openLocation(wx_object);
  };

  wx.getLocation = function getLocation(wx_object) {
    return tt.getLocation(wx_object);
  };

  wx.chooseLocation = function chooseLocation(wx_object) {
    return tt.chooseLocation(wx_object);
  };

  // //////// Media ////////////////////


  wx.createMapContext = function createMapContext(id) {
    return getApp().onekit.context[id];
  };

  wx.compressImage = function compressImage(wx_object) {
    return tt.compressImage(wx_object);
  };

  wx.saveImageToPhotosAlbum = function saveImageToPhotosAlbum(wx_object) {
    return tt.saveImageToPhotosAlbum(wx_object);
  };

  wx.getImageInfo = function getImageInfo(wx_object) {
    return tt.getImageInfo(wx_object);
  };

  wx.previewImage = function previewImage(wx_object) {
    return tt.previewImage(wx_object);
  };

  wx.chooseImage = function chooseImage(wx_object) {
    return tt.chooseImage(wx_object);
  };

  wx.saveVideoToPhotosAlbum = function saveVideoToPhotosAlbum(wx_object) {
    return tt.saveVideoToPhotosAlbum(wx_object);
  };

  wx.chooseVideo = function chooseVideo(wx_object) {
    return tt.chooseVideo(wx_object);
  };

  wx.createVideoContext = function createVideoContext(wx_object) {
    return tt.createVideoContext(wx_object);
  };

  wx.createLivePlayerContext = function createLivePlayerContext(wx_object) {
    return tt.createLivePlayerContext(wx_object);
  };

  wx.createCameraContext = function createCameraContext(wx_object) {
    return tt.createCameraContext(wx_object);
  };

  wx.stopVoice = function stopVoice(wx_object) {
    return tt.stopVoice(wx_object);
  };

  wx.pauseVoice = function pauseVoice(wx_object) {
    return tt.pauseVoice(wx_object);
  };

  wx.playVoice = function playVoice(wx_object) {
    return tt.playVoice(wx_object);
  };

  wx.setInnerAudioOption = function setInnerAudioOption(wx_object) {
    return tt.setInnerAudioOption(wx_object);
  };

  wx.getAvailableAudioSources = function getAvailableAudioSources(wx_object) {
    return tt.getAvailableAudioSources(wx_object);
  };

  wx.createInnerAudioContext = function createInnerAudioContext(wx_object) {
    return tt.createInnerAudioContext(wx_object);
  };

  // static createAudioContext(wx_object) {
  //   return tt.createAudioContext(wx_object)
  // }

  wx.onBackgroundAudioStop = function onBackgroundAudioStop(wx_object) {
    return tt.onBackgroundAudioStop(wx_object);
  };

  wx.onBackgroundAudioPause = function onBackgroundAudioPause(wx_object) {
    return tt.onBackgroundAudioPause(wx_object);
  };

  wx.onBackgroundAudioPlay = function onBackgroundAudioPlay(wx_object) {
    return tt.onBackgroundAudioPlay(wx_object);
  };

  wx.stopBackgroundAudio = function stopBackgroundAudio(wx_object) {
    return tt.stopBackgroundAudio(wx_object);
  };

  wx.seekBackgroundAudio = function seekBackgroundAudio(wx_object) {
    return tt.seekBackgroundAudio(wx_object);
  };

  wx.pauseBackgroundAudio = function pauseBackgroundAudio(wx_object) {
    return tt.pauseBackgroundAudio(wx_object);
  };

  wx.playBackgroundAudio = function playBackgroundAudio() {
    /* return tt.playBackgroundAudio(wx_object) */
  };

  wx.getBackgroundAudioPlayerState = function getBackgroundAudioPlayerState() {
    /* return tt.getBackgroundAudioPlayerState(wx_object) */
  };

  wx.getBackgroundAudioManager = function getBackgroundAudioManager(wx_object) {
    return tt.getBackgroundAudioManager(wx_object);
  };

  wx.createLivePusherContext = function createLivePusherContext(wx_object) {
    return tt.createLivePusherContext(wx_object);
  };

  wx.startRecord = function startRecord(wx_object) {
    var recorderManager = tt.getRecorderManager(wx_object);
    recorderManager.onStart(function () {
      var res = 'stopRecord才会返回tempFilePath!!';
      if (wx_object.success) {
        wx_object.success(res);
      }
      if (wx_object.complete) {
        wx_object.complete(res);
      }
    });
    var result = recorderManager.start();
    return result;
  };

  wx.stopRecord = function stopRecord(wx_object) {
    var recorderManager = tt.getRecorderManager(wx_object);
    recorderManager.onStop(function (res) {
      if (wx_object.success) {
        wx_object.success(res);
      }
      if (wx_object.complete) {
        wx_object.complete(res);
      }
    });
    var result = recorderManager.stop();
    return result;
  };

  wx.getRecorderManager = function getRecorderManager(wx_object) {
    return tt.getRecorderManager(wx_object);
  };

  // ////////////// Network ///////////////


  wx.request = function request(wx_object) {
    return tt.request(wx_object);
  };

  wx.downloadFile = function downloadFile(wx_object) {
    return tt.downloadFile(wx_object);
  };

  wx.uploadFile = function uploadFile(wx_object) {
    // tt.uploadFile({
    //   url: wx_object.url,
    //   filePath: wx_object.filePath,
    //   fileName: wx_object.name,
    //   fileType: 'image',
    //   header: wx_object.header,
    //   formData: wx_object.formData,
    //   success: wx_object.success,
    //   fail: wx_object.fail,
    //   complete: wx_object.complete
    // })
    return tt.uploadFile(wx_object);
  };

  //


  wx.connectSocket = function connectSocket(wx_object) {
    return new _WxSocketTask2.default(tt.connectSocket(wx_object));
  };

  wx.onSocketError = function onSocketError(wx_object) {
    return tt.onSocketError(wx_object);
  };

  wx.onSocketMessage = function onSocketMessage(wx_object) {
    return tt.onSocketMessage(wx_object);
  };

  wx.sendSocketMessage = function sendSocketMessage(wx_object) {
    return tt.sendSocketMessage(wx_object);
  };

  wx.closeSocket = function closeSocket(wx_object) {
    return tt.closeSocket(wx_object);
  };

  wx.offLocalServiceResolveFail = function offLocalServiceResolveFail(wx_object) {
    return tt.offLocalServiceResolveFail(wx_object);
  };

  wx.onLocalServiceResolveFail = function onLocalServiceResolveFail(wx_object) {
    return tt.onLocalServiceResolveFail(wx_object);
  };

  wx.offLocalServiceDiscoveryStop = function offLocalServiceDiscoveryStop(wx_object) {
    return tt.offLocalServiceDiscoveryStop(wx_object);
  };

  wx.onLocalServiceDiscoveryStop = function onLocalServiceDiscoveryStop(wx_object) {
    return tt.onLocalServiceDiscoveryStop(wx_object);
  };

  wx.offLocalServiceLost = function offLocalServiceLost(wx_object) {
    return tt.offLocalServiceLost(wx_object);
  };

  wx.onLocalServiceLost = function onLocalServiceLost(wx_object) {
    return tt.onLocalServiceLost(wx_object);
  };

  wx.offLocalServiceFound = function offLocalServiceFound(wx_object) {
    return tt.offLocalServiceFound(wx_object);
  };

  wx.onLocalServiceFound = function onLocalServiceFound(wx_object) {
    return tt.onLocalServiceFound(wx_object);
  };

  wx.stopLocalServiceDiscovery = function stopLocalServiceDiscovery() {
    /* return tt.stopLocalServiceDiscovery(wx_object) */
  };

  wx.startLocalServiceDiscovery = function startLocalServiceDiscovery() {}
  /*   return tt.startLocalServiceDiscovery(wx_object) */


  // /////// Open Interface //////////
  ;

  wx._checkSession = function _checkSession() {
    var now = new Date().getTime();
    return getApp().onekit._jscode && getApp().onekit._login && now <= getApp().onekit._login + 1000 * 60 * 60 * 24 * 3;
  };

  wx.checkSession = function checkSession(wx_object) {
    if (wx._checkSession()) {
      if (wx_object.success) {
        wx_object.success();
      }
      if (wx_object.complete) {
        wx_object.complete();
      }
    } else {
      if (wx_object.fail) {
        wx_object.fail();
      }
      if (wx_object.complete) {
        wx_object.complete();
      }
    }
  };

  wx.login = function login(wx_object) {
    if (!wx_object) {
      tt.login(wx_object);
      return;
    }
    var tt_object = {};
    tt_object.success = function (res) {
      getApp().onekit._jscode = res.code;
      getApp().onekit._login = new Date().getTime();
      var result = {
        code: res.code
      };
      if (wx_object.success) {
        wx_object.success(result);
      }
      if (wx_object.complete) {
        wx_object.complete(result);
      }
    };
    tt_object.fail = function (res) {
      if (wx_object.fail) {
        wx_object.fail(res);
      }
      if (wx_object.complete) {
        wx_object.complete(res);
      }
    };
    /* console.log(wx._checkSession())
    if (wx._checkSession()) {
      tt_object.success({
        code: getApp().onekit._jscode
      })
    } else { */
    tt.login(tt_object);
    // }
  };

  wx.getUserInfo = function getUserInfo(wx_object) {
    wx.login({
      success: function success(res) {
        var jscode = res.code;
        var withCredentials = wx_object.withCredentials === true;
        tt.getUserInfo({
          withCredentials: withCredentials,
          success: function success(res) {
            console.log(res);
            var url = getApp().onekit.server + 'userinfo';
            tt.request({
              url: url,
              header: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              method: 'POST',
              data: {
                withCredentials: withCredentials,
                data: JSON.stringify(res),
                js_code: jscode
              },
              success: function success(res) {
                if (wx_object.success) {
                  res.detail = res.data;
                  wx_object.success(res.data);
                }
                if (wx_object.complete) {
                  wx_object.complete(res);
                }
              },
              fail: function fail(res) {
                console.log(res);
              }
            });
          }
        });
      }
    });
  };

  wx.getOpenData = function getOpenData(wx_object) {
    if (!getApp().onekit._opendataCallbacks) {
      getApp().onekit._opendataCallbacks = [];
    }

    function _success(data) {
      var opendata = data.opendata;
      getApp().onekit._opendata = opendata;
      for (var cb = 0; cb < getApp().onekit._opendataCallbacks.length; cb++) {
        getApp().onekit._opendataCallbacks[cb](opendata);
      }
      if (wx_object.success) {
        wx_object.success(opendata);
      }
      if (wx_object.complete) {
        wx_object.complete(opendata);
      }
    }
    var opendata = getApp().onekit._opendata;
    if (opendata) {
      if (Object.keys(opendata) > 0) {
        wx_object.success(opendata);
      } else if (wx_object.success) {
        getApp().onekit._opendataCallbacks.push(wx_object.success);
      }
      return;
    }
    getApp().onekit._opendata = {};
    wx.login({
      success: function success(res) {
        var jscode = res.code;
        tt.getUserInfo({
          withCredentials: true,
          success: function success(res) {
            var url = getApp().onekit.server + 'opendata';
            tt.request({
              url: url,
              header: {
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              method: 'POST',
              data: {
                data: JSON.stringify(res),
                js_code: jscode
              },
              success: function success(res) {
                _success(res.data);
              },
              fail: function fail(res) {
                console.log(res);
              }
            });
          }
        });
      }
    });
  };

  wx._getPhoneNumber = function _getPhoneNumber(data, callback) {
    wx.login({
      success: function success(res) {
        var jscode = res.code;
        var url = getApp().onekit.server + 'phonenumber';
        console.log(data, jscode);
        tt.request({
          url: url,
          header: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          data: {
            data: JSON.stringify(data),
            js_code: jscode
          },
          success: function success(res) {
            var data = res.data;
            callback(data);
          },
          fail: function fail(res) {
            console.log(res.data);
          }
        });
      }
    });
  };

  wx.getPhoneNumber = function getPhoneNumber(wx_object) {
    var _this = this;

    getApp().onekit._bindgetphonenumber = function (data) {
      _this._getPhoneNumber(data, function (res) {
        if (wx_object.success) {
          wx_object.success(res);
        }
        if (wx_object.complete) {
          wx_object.complete(res);
        }
      });
    };
    tt.navigateTo({
      url: 'page/getphonenumber'
    });
  };

  wx.navigateToMiniProgram = function navigateToMiniProgram(wx_object) {
    return tt.navigateToMiniProgram(wx_object);
  };

  wx.navigateBackMiniProgram = function navigateBackMiniProgram(wx_object) {
    return tt.navigateBackMiniProgram(wx_object);
  };

  wx.getAccountInfoSync = function getAccountInfoSync(wx_object) {
    return tt.getAccountInfoSync(wx_object);
  };

  wx.reportMonitor = function reportMonitor() {
    /* return tt.reportMonitor(wx_object) */
  };

  wx.reportAnalytics = function reportAnalytics(wx_object, eventName) {
    return tt.reportAnalytics(wx_object, eventName);
  };

  wx.requestPayment = function requestPayment(wx_object) {
    var url = getApp().onekit.server + '/wx.requestPayment';
    console.log(url, wx_object);
    delete wx_object.success;
    delete wx_object.fail;
    delete wx_object.complete;
    tt.request({
      url: url,
      data: wx_object,
      header: {},
      method: 'POST',
      success: function success(res) {
        console.log(res);
        var tt_object = {
          orderInfo: res.data,
          service: 3,
          _debug: 1,
          success: wx_object.success,
          fail: wx_object.fail,
          complete: wx_object.complete
        };
        return tt.pay(tt_object);
      }
    });
  };

  wx.authorize = function authorize(wx_object) {
    return tt.authorize(wx_object);
  };

  wx.openSetting = function openSetting(wx_object) {
    return tt.openSetting(wx_object);
  };

  wx.getSetting = function getSetting(wx_object) {
    return tt.getSetting(wx_object);
  };

  wx.chooseAddress = function chooseAddress(wx_object) {
    return tt.chooseAddress(wx_object);
  };

  wx.openCard = function openCard(wx_object) {
    return tt.openCard(wx_object);
  };

  wx.addCard = function addCard(wx_object) {
    return tt.addCard(wx_object);
  };

  wx.chooseInvoiceTitle = function chooseInvoiceTitle(wx_object) {
    return tt.chooseInvoiceTitle(wx_object);
  };

  wx.chooseInvoice = function chooseInvoice(wx_object) {
    return tt.chooseInvoice(wx_object);
  };

  wx.startSoterAuthentication = function startSoterAuthentication(wx_object) {
    return tt.startSoterAuthentication(wx_object);
  };

  wx.checkIsSupportSoterAuthentication = function checkIsSupportSoterAuthentication(wx_object) {
    return tt.checkIsSupportSoterAuthentication(wx_object);
  };

  wx.checkIsSoterEnrolledInDevice = function checkIsSoterEnrolledInDevice(wx_object) {
    return tt.checkIsSoterEnrolledInDevice(wx_object);
  };

  wx.getWeRunData = function getWeRunData(wx_object) {
    return tt.getWeRunData(wx_object);
  };

  // //////// Router //////////////


  wx.navigateBack = function navigateBack(wx_object) {
    return tt.navigateBack(wx_object);
  };

  wx.switchTab = function switchTab(wx_object) {
    return tt.switchTab(wx_object);
  };

  wx.navigateTo = function navigateTo(wx_object) {
    return tt.navigateTo(wx_object);
  };

  wx.reLaunch = function reLaunch(wx_object) {
    return tt.reLaunch(wx_object);
  };

  wx.redirectTo = function redirectTo(wx_object) {
    return tt.redirectTo(wx_object);
  };

  // /////////// Share /////////////


  wx.updateShareMenu = function updateShareMenu(wx_object) {
    return tt.updateShareMenu(wx_object);
  };

  wx.showShareMenu = function showShareMenu(wx_object) {
    return tt.showShareMenu(wx_object);
  };

  wx.hideShareMenu = function hideShareMenu(wx_object) {
    return tt.hideShareMenu(wx_object);
  };

  wx.getShareInfo = function getShareInfo(wx_object) {
    return tt.getShareInfo(wx_object);
  };

  // ///////////// Storage //////////////


  wx.getStorageInfoSync = function getStorageInfoSync(wx_object) {
    return tt.getStorageInfoSync(wx_object);
  };

  wx.getStorageInfo = function getStorageInfo(wx_object) {
    return tt.getStorageInfo(wx_object);
  };

  wx.clearStorageSync = function clearStorageSync(wx_object) {
    return tt.clearStorageSync(wx_object);
  };

  wx.clearStorage = function clearStorage(wx_object) {
    return tt.clearStorage(wx_object);
  };

  wx.removeStorageSync = function removeStorageSync(wx_object) {
    return tt.removeStorageSync(wx_object);
  };

  wx.removeStorage = function removeStorage(wx_object) {
    return tt.removeStorage(wx_object);
  };

  wx.setStorageSync = function setStorageSync(key, value) {
    return tt.setStorageSync({
      key: key,
      data: value
    });
  };

  wx.setStorage = function setStorage(wx_object) {
    return tt.setStorage(wx_object);
  };

  wx.getStorageSync = function getStorageSync(key) {
    return tt.getStorageSync(key);
  };

  wx.getStorage = function getStorage(wx_object) {
    return tt.getStorage(wx_object);
  };

  // //////////// UI ////////////////


  wx.showActionSheet = function showActionSheet(wx_object) {
    return tt.showActionSheet(wx_object);
  };

  // static redirectTo(wx_object) { return tt.redirectTo(wx_object) }
  // static redirectTo(wx_object) { return tt.redirectTo(wx_object) }


  wx.hideLoading = function hideLoading(wx_object) {
    return tt.hideLoading(wx_object);
  };

  wx.showLoading = function showLoading(wx_object) {
    return tt.showLoading(wx_object);
  };

  wx.hideToast = function hideToast(wx_object) {
    return tt.hideToast(wx_object);
  };

  wx.showToast = function showToast(wx_object) {
    return tt.showToast(wx_object);
  };

  wx.showModal = function showModal(wx_object) {
    return tt.showModal(wx_object);
  };

  wx.setNavigationBarColor = function setNavigationBarColor(wx_object) {
    return tt.setNavigationBarColor(wx_object);
  };

  wx.hideNavigationBarLoading = function hideNavigationBarLoading(wx_object) {
    return tt.hideNavigationBarLoading(wx_object);
  };

  wx.showNavigationBarLoading = function showNavigationBarLoading(wx_object) {
    return tt.showNavigationBarLoading(wx_object);
  };

  wx.setNavigationBarTitle = function setNavigationBarTitle(wx_object) {
    return tt.setNavigationBarTitle(wx_object);
  };

  wx.setBackgroundTextStyle = function setBackgroundTextStyle(wx_object) {
    return tt.setBackgroundTextStyle(wx_object);
  };

  wx.setBackgroundColor = function setBackgroundColor() {
    /* return tt.setBackgroundColor(wx_object) */
  };

  wx.setTabBarItem = function setTabBarItem(wx_object) {
    return tt.setTabBarItem(wx_object);
  };

  wx.setTabBarStyle = function setTabBarStyle(wx_object) {
    return tt.setTabBarStyle(wx_object);
  };

  wx.hideTabBar = function hideTabBar(wx_object) {
    return tt.hideTabBar(wx_object);
  };

  wx.showTabBar = function showTabBar(wx_object) {
    return tt.showTabBar(wx_object);
  };

  wx.hideTabBarRedDot = function hideTabBarRedDot(wx_object) {
    return tt.hideTabBarRedDot(wx_object);
  };

  wx.showTabBarRedDot = function showTabBarRedDot(wx_object) {
    return tt.showTabBarRedDot(wx_object);
  };

  wx.removeTabBarBadge = function removeTabBarBadge(wx_object) {
    return tt.removeTabBarBadge(wx_object);
  };

  wx.setTabBarBadge = function setTabBarBadge(wx_object) {
    return tt.setTabBarBadge(wx_object);
  };

  wx.loadFontFace = function loadFontFace() {
    /* return tt.loadFontFace(wx_object) */
  };

  wx.stopPullDownRefresh = function stopPullDownRefresh(wx_object) {
    return tt.stopPullDownRefresh(wx_object);
  };

  wx.startPullDownRefresh = function startPullDownRefresh(wx_object) {
    return tt.startPullDownRefresh(wx_object);
  };

  wx.pageScrollTo = function pageScrollTo(wx_object) {
    return tt.pageScrollTo(wx_object);
  };

  wx.setTopBarText = function setTopBarText(wx_object) {
    return tt.setTopBarText(wx_object);
  };

  wx.nextTick = function nextTick(wx_object) {
    return tt.nextTick(wx_object);
  };

  wx.getMenuButtonBoundingClientRect = function getMenuButtonBoundingClientRect(wx_object) {
    return tt.getMenuButtonBoundingClientRect(wx_object);
  };

  wx.offWindowResize = function offWindowResize(wx_object) {
    return tt.offWindowResize(wx_object);
  };

  wx.onWindowResize = function onWindowResize(wx_object) {
    return tt.onWindowResize(wx_object);
  };

  // //////////// Worker ///////////////


  wx.createWorker = function createWorker(path) {
    return new _WORKER2.default(path);
  };

  // //////////// WXML ///////////////


  wx.createSelectorQuery = function createSelectorQuery(wx_object) {
    return tt.createSelectorQuery(wx_object);
  };

  wx.createIntersectionObserver = function createIntersectionObserver(wx_object) {
    return tt.createIntersectionObserver(wx_object);
  };

  // ///////////////////////////////////


  wx.hideKeyboard = function hideKeyboard(wx_object) {
    return tt.hideKeyboard(wx_object);
  };

  wx.setBackgroundFetchToken = function setBackgroundFetchToken() {};

  // ////////////////////////////////////
  // /////////// cloud ////////////////


  _createClass(wx, null, [{
    key: 'cloud',
    get: function get() {
      return new _wx2.default();
    }
  }]);

  return wx;
}();

exports.default = wx;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _onekit = __webpack_require__(0);

var _onekit2 = _interopRequireDefault(_onekit);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } } /* eslint-disable prefer-rest-params */


var CanvasContext = function () {
  function CanvasContext() {
    _classCallCheck(this, CanvasContext);

    this._actions = [];
    this._path = [];
  }

  CanvasContext.prototype.beginPath = function beginPath() {
    this._actions = [];
    this._path = [];
  };

  CanvasContext.prototype.save = function save() {
    this._actions.push({
      method: 'save',
      data: []
    });
  };

  CanvasContext.prototype.restore = function restore() {
    this._actions.push({
      method: 'restore',
      data: []
    });
  };

  //


  CanvasContext.prototype.setGlobalAlpha = function setGlobalAlpha(alpha) {
    this._actions.push({
      method: 'setGlobalAlpha',
      data: [alpha]
    });
  };

  CanvasContext.prototype.setFillStyle = function setFillStyle(color) {
    color = _onekit2.default.color.fix(color);
    this._actions.push({
      method: 'setFillStyle',
      data: ['normal', _onekit2.default.color.str2array(color)]
    });
  };

  CanvasContext.prototype.setStrokeStyle = function setStrokeStyle(color) {
    color = _onekit2.default.color.fix(color);
    this._actions.push({
      method: 'setStrokeStyle',
      data: ['normal', _onekit2.default.color.str2array(color)]
    });
  };

  CanvasContext.prototype.setShadow = function setShadow(x, y, blur, color) {
    color = _onekit2.default.color.fix(color);
    this._actions.push({
      method: 'setShadow',
      data: [x, y, blur, _onekit2.default.color.str2array(color)]
    });
  };

  CanvasContext.prototype.setLineCap = function setLineCap(cap) {
    this._actions.push({
      method: 'setLineCap',
      data: [cap]
    });
  };

  CanvasContext.prototype.setLineJoin = function setLineJoin(join) {
    this._actions.push({
      method: 'setLineJoin',
      data: [join]
    });
  };

  CanvasContext.prototype.setLineWidth = function setLineWidth(width) {
    this._actions.push({
      method: 'setLineWidth',
      data: [width]
    });
  };

  CanvasContext.prototype.setMiterLimit = function setMiterLimit(limit) {
    this._actions.push({
      method: 'setMiterLimit',
      data: [limit]
    });
  };

  CanvasContext.prototype.setFontSize = function setFontSize(size) {
    this._actions.push({
      method: 'setFontSize',
      data: [size]
    });
  };

  CanvasContext.prototype.rotate = function rotate(angle) {
    this._actions.push({
      method: 'rotate',
      data: [angle]
    });
  };

  CanvasContext.prototype.scale = function scale(sx, sy) {
    this._actions.push({
      method: 'scale',
      data: [sx, sy]
    });
  };

  CanvasContext.prototype.translate = function translate(tx, ty) {
    this._actions.push({
      method: 'translate',
      data: [tx, ty]
    });
  };

  CanvasContext.prototype.moveTo = function moveTo(x, y) {
    this._path.push({
      method: 'moveTo',
      data: [x, y]
    });
  };

  CanvasContext.prototype.lineTo = function lineTo(x, y) {
    this._path.push({
      method: 'lineTo',
      data: [x, y]
    });
  };

  CanvasContext.prototype.closePath = function closePath() {
    this._path.push({
      method: 'closePath',
      data: []
    });
  };

  CanvasContext.prototype.fillText = function fillText() {
    var data = [];
    for (var _iterator = arguments, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
      var _ref;

      if (_isArray) {
        if (_i >= _iterator.length) break;
        _ref = _iterator[_i++];
      } else {
        _i = _iterator.next();
        if (_i.done) break;
        _ref = _i.value;
      }

      var arg = _ref;

      data.push(arg);
    }
    this._actions.push({
      method: 'fillText',
      data: data
    });
  };

  CanvasContext.prototype.drawImage = function drawImage() {
    var data = [];
    for (var _iterator2 = arguments, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
      var _ref2;

      if (_isArray2) {
        if (_i2 >= _iterator2.length) break;
        _ref2 = _iterator2[_i2++];
      } else {
        _i2 = _iterator2.next();
        if (_i2.done) break;
        _ref2 = _i2.value;
      }

      var arg = _ref2;

      data.push(arg);
    }
    this._actions.push({
      method: 'drawImage',
      data: data
    });
  };

  CanvasContext.prototype.arc = function arc() {
    var data = [];
    for (var _iterator3 = arguments, _isArray3 = Array.isArray(_iterator3), _i3 = 0, _iterator3 = _isArray3 ? _iterator3 : _iterator3[Symbol.iterator]();;) {
      var _ref3;

      if (_isArray3) {
        if (_i3 >= _iterator3.length) break;
        _ref3 = _iterator3[_i3++];
      } else {
        _i3 = _iterator3.next();
        if (_i3.done) break;
        _ref3 = _i3.value;
      }

      var arg = _ref3;

      data.push(arg);
    }
    if (data.length < 6) {
      data.push(false);
    }
    this._path.push({
      method: 'arc',
      data: data
    });
  };

  CanvasContext.prototype.quadraticCurveTo = function quadraticCurveTo() {
    var data = [];
    for (var _iterator4 = arguments, _isArray4 = Array.isArray(_iterator4), _i4 = 0, _iterator4 = _isArray4 ? _iterator4 : _iterator4[Symbol.iterator]();;) {
      var _ref4;

      if (_isArray4) {
        if (_i4 >= _iterator4.length) break;
        _ref4 = _iterator4[_i4++];
      } else {
        _i4 = _iterator4.next();
        if (_i4.done) break;
        _ref4 = _i4.value;
      }

      var arg = _ref4;

      data.push(arg);
    }
    this._path.push({
      method: 'quadraticCurveTo',
      data: data
    });
  };

  CanvasContext.prototype.bezierCurveTo = function bezierCurveTo() {
    var data = [];
    for (var _iterator5 = arguments, _isArray5 = Array.isArray(_iterator5), _i5 = 0, _iterator5 = _isArray5 ? _iterator5 : _iterator5[Symbol.iterator]();;) {
      var _ref5;

      if (_isArray5) {
        if (_i5 >= _iterator5.length) break;
        _ref5 = _iterator5[_i5++];
      } else {
        _i5 = _iterator5.next();
        if (_i5.done) break;
        _ref5 = _i5.value;
      }

      var arg = _ref5;

      data.push(arg);
    }
    this._path.push({
      method: 'bezierCurveTo',
      data: data
    });
  };

  //


  CanvasContext.prototype.rect = function rect(x, y, width, height) {
    this._path.push({
      method: 'rect',
      data: [x, y, width, height]
    });
  };

  //


  CanvasContext.prototype.clearRect = function clearRect(x, y, width, height) {
    this._actions.push({
      method: 'clearRect',
      data: [x, y, width, height]
    });
  };

  CanvasContext.prototype.stroke = function stroke() {
    this._actions.push({
      method: 'strokePath',
      data: this._path
    });
  };

  CanvasContext.prototype.fill = function fill() {
    this._actions.push({
      method: 'fillPath',
      data: this._path
    });
  };

  CanvasContext.prototype.getActions = function getActions() {
    var actions = this._actions;
    this._actions = [];
    return actions;
  };

  return CanvasContext;
}();

exports.default = CanvasContext;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WORKER = function () {
  function WORKER(js) {
    _classCallCheck(this, WORKER);

    this._worker = new Worker(js);
  }

  WORKER.prototype.postMessage = function postMessage(message) {
    this._worker.postMessage(message);
  };

  WORKER.prototype.onMessage = function onMessage(callback) {
    this._worker.onmessage(callback);
  };

  WORKER.prototype.terminate = function terminate() {
    this._worker.terminate();
  };

  return WORKER;
}();

exports.default = WORKER;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* eslint-disable */

var wx_cloud_engine = function wx_cloud_engine() {
  _classCallCheck(this, wx_cloud_engine);
};

var wx_cloud_query = function () {
  function wx_cloud_query(collectionName, database, _query, _field) {
    _classCallCheck(this, wx_cloud_query);

    this.collectionName = collectionName;
    this.database = database;
    this._query = _query;
    this._field = _field;
  }

  wx_cloud_query.prototype.orderBy = function orderBy(fieldName, order) {
    var _query = this._query;
    if (!_query.order) {
      _query.order = [];
    }
    _query.order.push({ fieldName: fieldName, order: order });
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_query.prototype.limit = function limit(number) {
    var _query = this._query;
    _query.limit = number;
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_query.prototype.skip = function skip(number) {
    var _query = this._query;
    _query.skip = number;
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_query.prototype.count = function count(options) {
    var that = this;
    function _func(success, fail, complete) {
      basement.db.collection(that.collectionName).count(that._query.where).then(function (res2) {
        var res = {
          errMsg: 'collection.count:ok',
          total: res2.result
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud_query.prototype.field = function field(definition) {
    return new wx_cloud_query(this.collectionName, this.database, this._query, definition);
  };

  wx_cloud_query.prototype.get = function get(options) {
    var that = this;
    function _func(success, fail, complete) {
      basement.db.collection(that.collectionName).find(that._query.where, {
        sort: new Map(that._query.sort),
        limit: that._query.limit,
        skip: that._query.skip,
        projection: that._field
      }).then(function (res2) {
        var res = {
          errMsg: 'query.get:ok',
          data: res2.result
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  return wx_cloud_query;
}();

var wx_cloud_collection = function () {
  function wx_cloud_collection(database, collectionName) {
    _classCallCheck(this, wx_cloud_collection);

    this.database = database;
    this.collectionName = collectionName;
    this._query = {
      limit: null,
      offset: null,
      order: null,
      where: null
    };
    this._field = {};
  }

  wx_cloud_collection.prototype.doc = function doc(id) {
    return new wx_cloud_document(this, this.database, {}, id);
  };

  wx_cloud_collection.prototype.where = function where(rule) {
    var _query = this._query;
    _query.where = rule;
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_collection.prototype.add = function add(options) {
    var that = this;
    function _func(success, fail, complete) {
      options.data._openid = wx_cloud._openid;
      basement.db.collection(that.collectionName).insertOne(options.data).then(function (res2) {
        var res = {
          errMsg: 'collection.add:ok',
          _id: res2.result.insertedId
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud_collection.prototype.orderBy = function orderBy(fieldName, order) {
    var _query = this._query;
    _query.order = [{ fieldName: fieldName, order: order }];
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_collection.prototype.limit = function limit(number) {
    var _query = this._query;
    _query.limit = number;
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_collection.prototype.skip = function skip(number) {
    var _query = this._query;
    _query.skip = number;
    return new wx_cloud_query(this.collectionName, this.database, _query, this._field);
  };

  wx_cloud_collection.prototype.field = function field(definition) {
    return new wx_cloud_query(this.collectionName, this.database, this._query, definition);
  };

  wx_cloud_collection.prototype.get = function get(options) {
    var that = this;
    function _func(success, fail, complete) {
      basement.db.collection(that.collectionName).find({}, {
        sort: new Map(that._query.sort),
        limit: that._query.limit,
        skip: that._query.skip,
        projection: that._field
      }).then(function (res2) {
        var res = {
          errMsg: 'collection.get:ok',
          data: res2.result
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  return wx_cloud_collection;
}();

var wx_cloud_document = function () {
  function wx_cloud_document(collection, database, _field, id) {
    _classCallCheck(this, wx_cloud_document);

    this.collection = collection;
    this.database = database;
    this._field = _field;
    this._id = id;
  }

  wx_cloud_document.prototype.get = function get(options) {
    var that = this;
    function _func(success, fail, complete) {
      basement.db.collection(that.collection.collectionName).findOne({ _id: that._id }).then(function (res2) {
        var res = {
          errMsg: 'document.get:ok',
          data: res2.result
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud_document.prototype.update = function update(options) {
    var that = this;
    function _func(success, fail, complete) {
      basement.db.collection(that.collection.collectionName).updateOne({ _id: that._id }, { $set: options.data }).then(function (res2) {
        var res = {
          errMsg: 'docment.update:ok',
          stats: { updated: 1 }
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud_document.prototype.set = function set(options) {
    var that = this;
    function _func(success, fail, complete) {
      options.data = wx_cloud._openid;
      basement.db.collection(that.collection.collectionName).replaceOne({ _id: that._id }, { $set: options.data }).then(function (res2) {
        var res = {
          _id: that._id,
          errMsg: 'docment.set:ok',
          stats: { updated: 1, created: 0 }
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud_document.prototype.remove = function remove(options) {
    var that = this;
    function _func(success, fail, complete) {
      basement.db.collection(that.collection.collectionName).deleteOne({ _id: that._id }, { $set: options.data }).then(function (res2) {
        var res = {
          errMsg: 'docment.remove:ok',
          stats: { removed: 1 }
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud_document.prototype.field = function field(definition) {
    return new wx_cloud_query(this.collectionName, this.database, this._query, definition);
  };

  return wx_cloud_document;
}();

var wx_cloud_command = function () {
  function wx_cloud_command() {
    _classCallCheck(this, wx_cloud_command);
  }

  wx_cloud_command.prototype.geoNear = function geoNear(options) {};

  wx_cloud_command.prototype.geoWithin = function geoWithin(options) {};

  wx_cloud_command.prototype.geoIntersects = function geoIntersects(options) {};

  wx_cloud_command.prototype.eq = function eq(value) {
    return { $eq: value };
  };

  wx_cloud_command.prototype.neq = function neq(value) {
    return { $ne: value };
  };

  wx_cloud_command.prototype.lt = function lt(value) {
    return { $lt: value };
  };

  wx_cloud_command.prototype.lte = function lte(value) {
    return { $lte: value };
  };

  wx_cloud_command.prototype.gt = function gt(value) {
    return { $gt: value };
  };

  wx_cloud_command.prototype.gte = function gte(value) {
    return { $gte: value };
  };

  wx_cloud_command.prototype.in = function _in(value) {
    return { $in: value };
  };

  wx_cloud_command.prototype.nin = function nin(value) {
    return { $nin: value };
  };

  wx_cloud_command.prototype.and = function and(value) {
    return { $and: value };
  };

  wx_cloud_command.prototype.or = function or(value) {
    return { $or: value };
  };

  wx_cloud_command.prototype.set = function set(value) {
    return { $set: value };
  };

  wx_cloud_command.prototype.remove = function remove() {};

  wx_cloud_command.prototype.inc = function inc(value) {
    return { $inc: value };
  };

  wx_cloud_command.prototype.mul = function mul(value) {
    return { $mul: value };
  };

  wx_cloud_command.prototype.push = function push(value) {
    return { $push: value };
  };

  wx_cloud_command.prototype.pop = function pop(value) {
    return { $pop: value };
  };

  wx_cloud_command.prototype.shift = function shift(value) {
    return { $shift: value };
  };

  wx_cloud_command.prototype.unshift = function unshift(value) {
    return { $unshift: value };
  };

  return wx_cloud_command;
}();

var wx_cloud = function () {
  function wx_cloud() {
    _classCallCheck(this, wx_cloud);
  }

  wx_cloud.prototype.init = function init(options) {
    var that = this;
    var env = options.env;
    var traceUser = options.traceUser;
    /*
    basement.user.getInfo().then(user => {
      wx_cloud._openid = user.userId
    }).catch(console.error);
    */
  };

  wx_cloud.prototype.database = function database(options) {
    var env = void 0;
    if (options) {
      env = options.env;
    } else {
      env = null;
    }
    return new wx_cloud_database(env);
  };

  wx_cloud.prototype.callFunction = function callFunction(options) {
    return basement.function.invoke(options.name, options.data);
  };

  wx_cloud.prototype.uploadFile = function uploadFile(options) {
    var that = this;
    function _func(success, fail, complete) {
      var options2 = {
        filePath: options.filePath
      };
      if (options.header) {
        options2.headers = options.header;
      }
      basement.file.uploadFile(options2).then(function (res2) {
        var res = {
          errMsg: 'cloud.uploadFile:ok',
          statusCode: 200,
          fileID: res2.fileUrl
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      });
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud.prototype.getTempFileURL = function getTempFileURL(options) {
    var that = this;
    function _func(success, fail, complete) {
      var fileList = [];
      var func = function func(res2) {
        console.log(res2);
        fileList.push(res2.fileUrl);
        if (fileList.length < options.fileList.length) {
          return;
        }
        var res = {
          errMsg: 'cloud.getTempFileURL:ok',
          fileList: fileList
        };
        if (success) {
          success(res);
        }
        if (complete) {
          success(complete);
        }
      };
      for (var _iterator = options.fileList, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
        var _ref;

        if (_isArray) {
          if (_i >= _iterator.length) break;
          _ref = _iterator[_i++];
        } else {
          _i = _iterator.next();
          if (_i.done) break;
          _ref = _i.value;
        }

        var url = _ref;

        var res = basement.file.getPrivateFileByUrl({
          fileUrl: url
        }).then(func);
      }
    }
    if (options && options.success) {
      _func(options.success, options.fail, options.complete);
    } else {
      return new Promise(_func);
    }
  };

  wx_cloud.prototype.deleteFile = function deleteFile(options) {};

  return wx_cloud;
}();
// /////////////////////// database ///////////////////////


exports.default = wx_cloud;

var wx_cloud_database = function () {
  function wx_cloud_database(env) {
    _classCallCheck(this, wx_cloud_database);

    var _Decoder = void 0;var _QuerySerializer = void 0;var _UpdateSerializer = void 0;
    //
    this.config = { env: env };
    this.command = new wx_cloud_command();
    this.Geo = new wx_cloud_geo();
    this.engine = new wx_cloud_engine();
  }

  wx_cloud_database.prototype.collection = function collection(collectionName) {
    return new wx_cloud_collection(this, collectionName);
  };

  wx_cloud_database.prototype.RegExp = function RegExp(initOptions) {};

  wx_cloud_database.prototype.serverDate = function serverDate(options) {
    var timestamp = new Date().getTime();
    if (options) {
      if (options.offset) {
        timestamp += options.offset;
      }
    }
    return new Date(timestamp);
  };

  _createClass(wx_cloud_database, [{
    key: 'Decoder',
    get: function get() {
      return this._Decoder;
    },
    set: function set(Decoder) {
      this._Decoder = Decoder;
    }
  }, {
    key: 'QuerySerializer',
    get: function get() {
      return this._QuerySerializer;
    },
    set: function set(QuerySerializer) {
      this._QuerySerializer = QuerySerializer;
    }
  }, {
    key: 'UpdateSerializer',
    get: function get() {
      return this._UpdateSerializer;
    },
    set: function set(UpdateSerializer) {
      this._UpdateSerializer = UpdateSerializer;
    }
  }]);

  return wx_cloud_database;
}();

var wx_cloud_geo = function () {
  function wx_cloud_geo() {
    _classCallCheck(this, wx_cloud_geo);
  }

  wx_cloud_geo.prototype.Point = function Point(longitude, latitude) {};

  wx_cloud_geo.prototype.LineString = function LineString() {};

  wx_cloud_geo.prototype.Polygon = function Polygon() {};

  wx_cloud_geo.prototype.MultiLineString = function MultiLineString() {};

  wx_cloud_geo.prototype.MultiPolygon = function MultiPolygon() {};

  _createClass(wx_cloud_geo, null, [{
    key: 'cloud',
    get: function get() {
      return null;
    }
  }]);

  return wx_cloud_geo;
}();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* eslint-disable camelcase */
var WxSocketTask = function () {
  function WxSocketTask(tt_socketTask) {
    _classCallCheck(this, WxSocketTask);

    this.tt_socketTask = tt_socketTask;
  }

  WxSocketTask.prototype.onMessage = function onMessage() {
    if (getApp().onekit_onSocketonMessage) {
      getApp().onekit_onSocketonMessage();
    }
    return this.tt_socketTask.onMessage();
  };

  WxSocketTask.prototype.onClose = function onClose() {
    if (getApp().onekit_onSocketon) {
      getApp().onekit_onSocketonClose();
    }
    return this.tt_socketTask.onClose();
  };

  WxSocketTask.prototype.onError = function onError() {
    if (getApp().onekit_onSocketonError) {
      getApp().onekit_onSocketonError();
    }
    return this.tt_socketTask.onError();
  };

  WxSocketTask.prototype.onOpen = function onOpen() {
    if (getApp().onekit_onSocketonOpen) {
      getApp().onekit_onSocketonOpen();
    }
    return this.tt_socketTask.onOpen();
  };

  WxSocketTask.prototype.close = function close() {
    if (getApp().onekit_onSocketClose) {
      getApp().onekit_onSocketClose();
    }
    return this.tt_socketTask.close();
  };

  WxSocketTask.prototype.send = function send() {
    if (getApp().onekit_onSocketSend) {
      getApp().onekit_onSocketSend();
    }
    return this.tt_socketTask.send();
  };

  return WxSocketTask;
}();

exports.default = WxSocketTask;

/***/ }),
/* 6 */,
/* 7 */,
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _wx = __webpack_require__(1);

var _wx2 = _interopRequireDefault(_wx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Component({
  mixins: [],
  data: {
    hideContact: true
  },
  properties: {},
  methods: {
    contactBG_tap: function contactBG_tap() {
      this.setData({
        hideContact: true
      });
    },
    button_tap: function button_tap(e) {
      var that = this;
      console.log('button', this.properties);
      if (this.properties.openType) {
        switch (this.properties.openType) {
          case 'contact':
            this.setData({
              hideContact: false
            });
            break;
          case 'share':
            _wx2.default.showShareMenu({
              success: function success() {}
            });
            break;
          case 'getUserInfo':
            _wx2.default.getUserInfo({
              success: function success(res) {
                e.detail = res;
                that.triggerEvent('Getuserinfo', e);
              }
            });

            break;
          case 'getPhoneNumber':
            _wx2.default.getPhoneNumber({
              success: function success(res) {
                e.detail = res;
                that.triggerEvent('Getphonenumber', e);
              }
            });

            break;
          case 'launchApp':
            break;
          case 'openSetting':
            _wx2.default.openSetting({});
            break;
          case 'feedback':
            break;
          default:
            console.error(this.properties.openType);
            break;
        }
      }
      this.triggerEvent('Tap', {});
    }
  }
}); /* eslint-disable no-console */

/***/ })
/******/ ]);