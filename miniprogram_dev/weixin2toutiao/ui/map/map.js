module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 19);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint-disable max-len */
/* eslint-disable camelcase */

var bd_USER_FOLDER = 'bdfile://store/';
var tt_USER_FOLDER = tt.env.USER_DATA_PATH + '/';

function current() {
  var pages = getCurrentPages();
  if (pages.length === 0) {
    return {};
  }
  return pages[pages.length - 1];
}

function currentUrl() {
  return current().route;
}

function new_bd_filePath(ext) {
  var randomString = Math.floor(Math.random() * (1 - 10000000) + 10000000);
  var bd_filePath = '' + bd_USER_FOLDER + randomString + ext;
  return bd_filePath;
}

function bd_filePath2tt_filePath(bd_filePath) {
  // eslint-disable-next-line no-undef
  if (!getApp().bdSavePath2ttStorePath) {
    return bd_filePath;
  }
  // eslint-disable-next-line no-undef
  var tt_storePath = getApp().bdSavePath2ttStorePath[bd_filePath];
  if (tt_storePath) {
    return tt_storePath;
  } else {
    var tt_filePath = bd_filePath.replace(bd_USER_FOLDER, tt_USER_FOLDER);
    return tt_filePath;
  }
}

function save_tt_storePath(bd_filePath, tt_storePath) {
  // eslint-disable-next-line no-undef
  if (!getApp().bdSavePath2ttStorePath) {
    // eslint-disable-next-line no-undef
    getApp().bdSavePath2ttStorePath = {};
  }
  // eslint-disable-next-line no-undef
  getApp().bdSavePath2ttStorePath[bd_filePath] = tt_storePath;
  // ///////////////////////
  // eslint-disable-next-line no-undef
  if (!getApp().ttStorePath2bdSavePath) {
    // eslint-disable-next-line no-undef
    getApp().ttStorePath2bdSavePath = {};
  }
  // eslint-disable-next-line no-undef
  getApp().ttStorePath2bdSavePath[tt_storePath] = bd_filePath;
}
module.exports = {
  current: current,
  currentUrl: currentUrl,
  save_tt_storePath: save_tt_storePath,
  new_bd_filePath: new_bd_filePath,
  bd_filePath2tt_filePath: bd_filePath2tt_filePath
};

/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _onekit = __webpack_require__(0);

var _onekit2 = _interopRequireDefault(_onekit);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Component({
  mixins: [],
  data: {},
  properties: {
    longitude: {
      type: Number,
      value: 116.406568
    },
    latitude: {
      type: Number,
      value: 39.914935
    },
    scale: {
      type: Number,
      value: 16
    },
    markers: {
      type: Array

    },
    covers: {
      type: Array
    },
    polyline: {
      type: Array
    },
    polygons: {
      type: Array
    },
    circles: {
      type: Array
    },
    controls: {
      type: Array
    },
    includePoints: {
      type: Array
    },
    showLocation: {
      type: Boolean,
      value: false
    },
    subkey: {
      type: String
    },
    layerStyle: {
      type: Number,
      value: 1
    },
    rotate: {
      type: Number,
      value: 0
    },
    skew: {
      type: Number,
      value: 0
    },
    enable3D: {
      type: Boolean,
      value: false
    },
    showCompass: {
      type: Boolean,
      value: false
    },
    showScale: {
      type: Boolean,
      value: false
    },
    enableOverlooking: {
      type: Boolean,
      value: false
    },
    enableZoom: {
      type: Boolean,
      value: true
    },
    enableScroll: {
      type: Boolean,
      value: true
    },
    enableRotate: {
      type: Boolean,
      value: false
    },
    enableSatellite: {
      type: Boolean,
      value: false
    },
    enableTraffic: {
      type: Boolean,
      value: false
    },
    setting: {
      type: Object
    }
  },
  attached: function attached() {
    getApp().onekit.webview = this;
    // //////////////////////////////////////
    var properties = {};
    for (var name in this.properties) {
      var value = this.properties[name];
      if (value != null) {
        properties[name] = value;
      }
    }
    console.log(properties);
    // properties.enable3D = true;
    var guid = getApp().onekit.appid; // onekit.guid();
    // console.log(encodeURIComponent(JSON.stringify(properties)));
    // console.log(guid);
    // let url = `http://localhost/weixin2toutiao/app/map.html?guid=${guid}&properties=${encodeURIComponent(JSON.stringify(properties))}`;
    // let url = `http://localhost/weixin2toutiao/app/map.html?guid=${guid}&properties=${encodeURIComponent(JSON.stringify(properties))}`;
    // let url = `https://www.onekit.com/weixin2toutiao/app/map.html?guid=${guid}&properties=${encodeURIComponent(JSON.stringify(properties))}`;
    var url = 'https://www.onekit.com/weixin2toutiao/app/map.html?guid=' + guid + '&properties=' + encodeURIComponent(JSON.stringify(properties));
    this.setData({
      url: url
    });
    // //////////////////////////////////////////////////

    this.data.ucid = _onekit2.default.guid();
    this.appSocket = getApp().onekit.socket;
    if (this.id) {
      var context = getApp().onekit.context[this.id] = {};
      context.getCenterLocation = this.getCenterLocation;
      context.getRegion = this.getRegion;
      context.getRotate = this.getRotate;
      context.getScale = this.getScale;
      context.getSkew = this.getSkew;
      context.includePoints = this.includePoints;
      context.moveToLocation = this.moveToLocation;
      context.setCenterOffset = this.setCenterOffset;
      context.translateMarker = this.translateMarker;
    }
    // //////////////////////////////////////////////////
  },
  didUpdate: function didUpdate() {},
  didUnmount: function didUnmount() {},

  methods: {
    socketSend: function socketSend(method, params, callbacks) {
      var data = {
        method: method,
        ucid: this.data.ucid,
        data: params
        //
      };this.appSocket.send(data, function (res) {
        if (callbacks.success) {
          callbacks.success(res);
        }
        if (callbacks.complete) {
          callbacks.complete(res);
        }
      });
    },
    getCenterLocation: function getCenterLocation(object) {
      this.socketSend('getCenterLocation', {}, object);
    },
    getRegion: function getRegion(object) {
      this.socketSend('getRegion', {}, object);
    },
    getRotate: function getRotate(object) {
      this.socketSend('getRotate', {}, object);
    },
    getScale: function getScale(object) {
      this.socketSend('getScale', {}, object);
    },
    getSkew: function getSkew(object) {
      this.socketSend('getSkew', {}, object);
    },
    includePoints: function includePoints(object) {
      var data = {
        points: object.points
      };
      if (object.padding) {
        data.padding = object.padding;
      }
      this.socketSend('includePoints', data, object);
    },
    moveToLocation: function moveToLocation(object) {
      var data = {};
      if (object.longitude && object.latitude) {
        data.longitude = object.longitude;
        data.latitude = object.latitude;
      }
      this.socketSend('moveToLocation', data, object);
    },
    setCenterOffset: function setCenterOffset() {
      console.warn('[onekit-map]暂不支持setCenterOffset');
    },
    translateMarker: function translateMarker(object) {
      var data = {
        markerId: object.markerId,
        destination: object.destination,
        autoRotate: object.autoRotate,
        rotate: object.rotate
      };
      if (object.duration) {
        data.duration = object.duration;
      }
      if (object.animationEnd) {
        data.animationEnd = object.animationEnd;
      }
      console.log(object);
      this.socketSend('translateMarker', data, object);
    }
  }

}); /* eslint-disable guard-for-in */
/* eslint-disable no-console */
// import AppSocket from '../../api/AppSocket'

/***/ })

/******/ });