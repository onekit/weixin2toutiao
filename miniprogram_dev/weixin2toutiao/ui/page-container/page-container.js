module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 27);
/******/ })
/************************************************************************/
/******/ ({

/***/ 27:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint-disable no-bitwise */
/* eslint-disable no-console */
Component({
  data: {
    overlay_color: 'rgb(0, 0, 0, 0.7)',
    keyframes: null,
    overlay_keyframes: null
  },
  properties: {
    show: {
      type: Boolean,
      value: false,
      observer: function observer(newVal, oldVal) {
        console.log('observer新值:', newVal, '旧值:', oldVal);
      }
    },
    duration: {
      type: Number,
      value: 300
    },
    zIndex: {
      type: Number,
      value: 100
    },
    overlay: {
      type: Boolean | String,
      value: true
    },
    position: {
      type: String,
      value: 'bottom'
    },
    round: {
      type: Boolean,
      value: false
    },
    closeOnSlideDown: {
      type: Boolean,
      value: false
    },
    overlayStyle: {
      type: String
    },
    customStyle: {
      type: String
    }
  },
  methods: {
    touchStart: function touchStart(e) {
      this.startY = e.touches[0].pageY;
    },
    touchMove: function touchMove(e) {
      this.endY = e.touches[0].pageY;
    },
    touchEnd: function touchEnd() {
      if (this.data.closeOnSlideDown && this.endY - this.startY >= 50) {
        this.setData({
          overlay_keyframes: 'overlay'
        });
        switch (this.data.position) {
          case 'top':
            this.setData({
              keyframes: 'top'
            });
            break;
          case 'bottom':
            this.setData({
              keyframes: 'bottom'
            });
            break;
          case 'center':
            this.setData({
              keyframes: 'cneter'
            });
            break;
          case 'right':
            this.setData({
              keyframes: 'right'
            });
            break;
          default:
            break;
        }

        // setTimeout(() => {
        //   this.setData({
        //     show: false,
        //     keyframes: null,
        //     overlay_keyframes: null
        //   })
        // }, this.data.duration)
        this.setData({
          show: false
        });
      }
      console.log('touchEnd', this.data.show);
    },
    click_overlay: function click_overlay() {
      this.triggerEvent('clickoverlay', this);
    }
  },
  lifetimes: {
    attached: function attached() {
      console.log('attached', this.data.show);
      var data = {};
      switch (this.data.position) {
        case 'bottom':
          data.set_position = 'bottom';
          if (this.data.round) {
            data.round_style = 'border-radius:20px 20px 0 0;overflow:hidden';
          }
          break;
        case 'top':
          data.set_position = 'top';
          if (this.data.round) {
            data.round_style = 'border-radius:0 0 20px 20px;overflow:hidden';
          }
          break;
        case 'center':
          data.set_position = 'top';
          data.overlay_color = '#FFFFFF';
          break;
        case 'right':
          data.set_position = 'top';
          data.overlay_color = '#FFFFFF';
          break;
        default:
          break;
      }

      if (this.data.overlay === 'false') {
        data.overlay_color = '#FFFFFF';
      }
      this.setData(data);
    }
  },
  observers: {
    show: function show(value) {
      var _this = this;

      if (value) {
        this.triggerEvent('beforeenter', this);
        this.triggerEvent('enter', this);
        setTimeout(function () {
          _this.triggerEvent('afterenter', _this);
        }, this.data.duration);
      } else if (!value) {
        this.triggerEvent('beforeleave', this);
        this.triggerEvent('leave', this);
        setTimeout(function () {
          _this.triggerEvent('afterleave', _this);
        }, this.data.duration);
      }
    }
  }
});

/***/ })

/******/ });